<?php

class m160817_125653_news_update extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `news` CHANGE `column` `column` VARCHAR(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL;');
	}

	public function down()
	{
		echo "m160817_125653_news_update does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}