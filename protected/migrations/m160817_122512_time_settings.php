<?php

class m160817_122512_time_settings extends CDbMigration
{
	public function up()
	{
	    $this->execute("
CREATE TABLE `day` (
  `id` int(11) NOT NULL,
  `title` enum('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `day`
--

INSERT INTO `day` (`id`, `title`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Sunday', 1, 1, '2016-08-03 17:36:06', '2016-08-03 17:40:47'),
(2, 'Monday', 1, NULL, '2016-08-03 17:36:17', NULL),
(3, 'Tuesday', 1, NULL, '2016-08-03 17:36:36', NULL),
(4, 'Wednesday', 1, NULL, '2016-08-03 17:36:38', NULL),
(5, 'Thursday', 1, NULL, '2016-08-03 17:36:41', NULL),
(6, 'Friday', 1, NULL, '2016-08-03 17:36:44', NULL),
(7, 'Saturday', 1, NULL, '2016-08-03 17:36:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `days_settings`
--

CREATE TABLE `days_settings` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `platform_category_settings_id` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `platform_category_settings`
--

CREATE TABLE `platform_category_settings` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings_obj`
--

CREATE TABLE `settings_obj` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `time_settings`
--

CREATE TABLE `time_settings` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `platform_category_settings_id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `is_direct_push` tinyint(1) NOT NULL DEFAULT '0',
  `direct_push_start_time` time DEFAULT NULL,
  `direct_push_end_time` time DEFAULT NULL,
  `gap_time` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `day`
--
ALTER TABLE `day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days_settings`
--
ALTER TABLE `days_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_category_settings_id` (`platform_category_settings_id`),
  ADD KEY `day_id` (`day_id`);

--
-- Indexes for table `platform_category_settings`
--
ALTER TABLE `platform_category_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `platform_id` (`platform_id`);

--
-- Indexes for table `settings_obj`
--
ALTER TABLE `settings_obj`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_settings`
--
ALTER TABLE `time_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `day`
--
ALTER TABLE `day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `days_settings`
--
ALTER TABLE `days_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `platform_category_settings`
--
ALTER TABLE `platform_category_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings_obj`
--
ALTER TABLE `settings_obj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `time_settings`
--
ALTER TABLE `time_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
	}

	public function down()
	{
		echo "m160817_122512_time_settings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}