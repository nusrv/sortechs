<?php

class m161130_115602_change_section_url_is_null extends CDbMigration
{
	public function up(){

        $this->execute("ALTER TABLE `category` CHANGE `url` `url` VARCHAR(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL;");
	}

	public function down()
	{
		echo "m161130_115602_change_section_url_is_null does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}