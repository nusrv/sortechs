<?php
class TwitterCommand extends BaseCommand{

    private $post = null;
    private $id = null;
    private $error = null;

    public function run($args){
        $this->TimeZone();
        $value =$this->News();

        if(!empty($value)){

            //catch the post before posting processing...
            $value->is_posted=3;
            $value->save(false);

           /* if(!empty($value->news_id)){

                $this->check_and_update_news($value->news_id,$value->id,$value->platform_id);

                $value=PostQueue::model()->findByPk($value->id);

            }*/
            

                $this->id = $value->id;
                $valid = false;
                $id = null;
                switch ($value->type) {
                    case 'Image':
                        $id =$this->image($value);
                        if($id)
                            $valid = true;
                        break;
                    case 'Video':
                        $this->post = $value;
                        $id =$this->video();
                        if($id){
                            $valid = true;
                        }
                        break;
                    case 'Text':
                        $id =$this->text($value);
                        if($id)
                            $valid = true;
                        break;
                    case 'Preview':
                        $id =$this->preview($value);
                        if($id)
                            $valid = true;
                        break;
                }
                if($valid){
                    $value->is_posted = 1;
                    $value->post_id = $id;
                    $value->command = false;
                    $value->save();
                }else{
                    $value->errors = $this->error;
                    $value->is_posted = 2;
                    $value->command = false;
                    $this->send_email('Twitter','error on Twitter posts');

                    $value->save();
                }
            }

        die;

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @return PostQueue the News
     * @var $data PostQueue
     * @var $value PostQueue
     * @var $db PostQueue
     */

    private function News()
    {

        return PostQueue::model()->get_post_twitter();

    }


    private function video(){
        $params = array(
            'status' => $this->post->post,
        );
        $obj = $this->Obj_twitter();

        $valid = false;

        if(!empty($this->post->media_url)){

            $video = Yii::app()->params['webroot'].'/image/twitter_video.mp4';

            file_put_contents($video, fopen($this->post->media_url, 'r'));

            sleep(1);


            if($this->Size_video($video)){


                echo 'Size_video';

                $size_bytes = filesize($video);
                $fp         = fopen($video, 'r');

                $reply = $obj->media_upload([
                    'command'     => 'INIT',
                    'media_type'  => 'video/mp4',
                    'total_bytes' => $size_bytes
                ]);

                if(isset($reply->media_id_string)){
                    $media_id = $reply->media_id_string;

                    $segment_id = 0;

                    while (!feof($fp)) {

                        $chunk = fread($fp, 3048576);

                        $reply = $obj->media_upload([
                            'command'       => 'APPEND',
                            'media_id'      => $media_id,
                            'segment_index' => $segment_id,
                            'media'         => $chunk
                        ]);

                        $segment_id++;

                        print_r($reply);
                    }
                    fclose($fp);

                    sleep(1);

                    $reply = $obj->media_upload([
                        'command'       => 'FINALIZE',
                        'media_id'      => $media_id
                    ]);

                    print_r($reply);

                    if ($reply->httpstatus < 200 || $reply->httpstatus > 299) {
                        return false;
                    }
                    $reply = $obj->statuses_update([
                        'status'    => $this->post->post,
                        'media_ids' => $media_id
                    ]);

                    print_r($reply);

                    return $reply->id_str;


                }else{
                    $reply = $obj->statuses_update($params);

                    $go = $reply->httpstatus != 200 or isset($reply->errors);

                    if ($go) {

                        return false;
                    } else {

                        return $reply->id_str;
                    }
                }

            }else{
                $valid = true;
            }
        }

        if($valid){
            $reply = $obj->statuses_update($params);
        }

        $go = $reply->httpstatus != 200 or isset($reply->errors);

        if ($go) {

            return false;
        } else {

            return $reply->id_str;
        }
    }

    private function image($value){
        $params = array(
            'status' => $value->post,
        );

        $obj = $this->Obj_twitter();

        $valid = true;

        if(!empty($value->media_url)){



            $image = Yii::app()->params['webroot'].'/image/twitter.jpg';



                if(@file_get_contents(urldecode($value->media_url))){
                    file_put_contents($image,file_get_contents(urldecode($value->media_url)));
                }else{
                    $this->error = 'failed to open stream: HTTP request failed! HTTP/1.1 404 Not Found';
                    $valid= false;
                }
            $this->image_quality($image);



            if($valid){
                $params['media[]']= $image;
                $reply = $obj->statuses_updateWithMedia($params);
            }

        }else{
            $reply = $obj->statuses_update($params);
        }

        if($valid){
            if(!empty($reply)){
                $go = $reply->httpstatus != 200 or isset($reply->errors);

                if ($go) {
                    print_r($reply);
                    echo PHP_EOL;
                    return false;
                } else {

                    return $reply->id_str;
                }
            }else{
                return false;
            }
        }else{
            echo  $this->error.PHP_EOL;
            return false;
        }
    }

    private function text($value)
    {
        $params = array(
            'status' => $value->post,
        );

        $obj = $this->Obj_twitter();

        $reply = $obj->statuses_update($params);

        $go = $reply->httpstatus != 200 or isset($reply->errors);

        if ($go) {
            print_r($reply);
            echo PHP_EOL;
            return false;
        } else {
            return $reply->id_str;
        }

    }

    private function preview($value)
    {
        if(preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$value->post,$matches))
        {
            $params = array('status' => $value->post);
        }else{
            $params = array('status' => $value->post.PHP_EOL.$value->link);
        }

        $obj = $this->Obj_twitter();

        $reply = $obj->statuses_update($params);

        $go = $reply->httpstatus != 200 or isset($reply->errors);

        if ($go) {
            print_r($reply);
            echo PHP_EOL;
            return false;
        } else {
            return $reply->id_str;
        }

    }

}