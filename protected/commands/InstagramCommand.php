<?php
class InstagramCommand extends BaseCommand
{
    private $id = null;

    private $id_instagram = null;

    private $post = null;

    private $error= null;

    public function run($args)
    {
        $this->TimeZone();
        $data = $this->News();


        if (!empty($data)) {
            foreach ($data as $value) {

                //catch the post before posting processing...
                $value->is_posted=3;
                $value->save(false);

                /*if(!empty($value->news_id)){

                    $this->check_and_update_news($value->news_id,$value->id,$value->platform_id);

                    $value=PostQueue::model()->findByPk($value->id);

                }*/

                $this->id = $value->id;
                $valid = false;
                switch ($value->type) {
                    case 'Image':
                        $this->post = $value;
                        if ($this->image()) {
                            $valid = true;
                        }
                        break;
                    case 'Video':
                        $this->post = $value;
                        //  if( $this->video()){
                        // $valid = true;
                        // }
                        break;
                }

                if ($valid) {
                    $value->is_posted = 1;
                    $value->post_id = $this->id_instagram;
                    $value->command = false;
                    $value->save();
                }else{
                    $value->is_posted = 2;
                    $value->errors= $this->error;
                    $value->command = false;
                    //$this->send_email('instagram','error on instagram');

                    $value->save();
                }
                break;
            }
        }

        die;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @return PostQueue the News
     * @var $data PostQueue
     * @var $value PostQueue
     * @var $db PostQueue
     */

    public function News()
    {

        return PostQueue::model()->get_post_instagram();

    }

    public function image()
    {
        $valid = true;
        if (!empty($this->post->media_url)) {



            $image = Yii::app()->params['webroot'].'/image/instagram.jpg';


                if(@file_get_contents(urldecode($this->post->media_url))){
                    file_put_contents($image,file_get_contents(urldecode($this->post->media_url)));

                    //$this->make_image_square($image,1);
                    $this->scaleImage($image);
                }else{
                    $this->error = 'failed to open stream: HTTP request failed! HTTP/1.1 404 Not Found';
                    $this->send_email('Instagram','error on instagram image');

                    $valid= false;
                }
            $this->image_quality($image);



        } else {
            $this->error = 'Image empty';
            $this->send_email('Instagram','empty image');

            return false;
        }

        if($valid){
            $data = $this->instagram_image($image,$this->post->post, Yii::app()->params['instagram_auth']);

            if(isset($data['status']) && $data['status'] == 'ok'){
                $this->id_instagram = $data['media']['id'];
                return true;
            }else{
                return false;
            }
        }

        return false;

    }


}
