<?php
class TestsCommand extends BaseCommand
{
    public function run()
    {
        $emails = Emails::model()->findAll();
        foreach ($emails as $email) {
            $this->send_Pdf_email('Sortechs - System', $email->f_name, $email->email, 'This is PDF file', 'Your PDF file', 'http://s3.amazonaws.com/sortechs/anazahra/pdf/pdf_1465913729.pdf');
        }
    }
    public function send_Pdf_email($user_downloaded_pdf,$name,$email,$subject,$MSGHTML,$link){

        $mail = new JPhpMailer;

        $mail->SMTPAuth = true;
        /* $mail->Username = 'Admin';
         $mail->Password = '123';*/
        $mail->SetFrom('system@sortechs.com', $user_downloaded_pdf);
        $mail->Subject = $subject;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML('<a href="'.$link.'">'.$MSGHTML.'</a>');
        /*        $mail->AddAttachment('http://www.anazahra.com/wp-content/gallery/lamia-abdeen-abayas14062016/ubuntu-tribe-collection-14.jpg','Pic');*/
        $mail->AddAddress($email, $name);
        $mail->Send();

    }
}
?>