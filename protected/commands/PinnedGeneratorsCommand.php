<?php
//Yii::import('application.commands.BaseCommand');
class PinnedGeneratorsCommand extends BaseCommand{

    private $id = null;

    private $id_fb = null;

    private $post = null;

    private $all_news;

    private $schedule_date;
    protected $details;
    protected $category;
    protected $model;
    protected $trim_hash;
    protected $hash_tag;
    protected $platform;
    protected $platform_id;
    protected $parent = null;
    protected $creator;
    protected $sub_category;
    protected $source;
    protected $news;
    protected $PostQueue;
    protected $post_id;

    public function run($args)
    {
        $this->TimeZone();
        $data = $this->News();
        $this->all_news = $this->getAllNews();
        $this->schedule_date = date('Y-m-d H:i:s');


        if(!empty($data)){
            foreach ($data as $value) {

                if(!empty($value->news_id)){

                    $this->check_and_update_news($value->news_id,$value->id,$value->platform_id);

                    $value=PostQueue::model()->findByPk($value->id);

                }
            }
        }
    }

    public function clearTag($string){

        $text = str_ireplace('# #', '#', $string);
        $text = str_ireplace('##', '#', $text);
        $text = str_ireplace('&#8220;', '', $text);
        $text = str_ireplace('&#8221;', '', $text);
        $text = str_ireplace('&#8230;', '', $text);
        $text = str_ireplace('&#x2014;', '', $text);
        $text = str_ireplace('#8211;', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('&nbsp;', '', $text);
        $text = str_ireplace('&#160;', '', $text);
        $text = str_ireplace('. .', '', $text);
        $text = str_ireplace('&#xf2;,', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('2&#xb0;', '', $text);
        return str_ireplace('&ndash;', '', $text);
    }

    private function News(){

        return PostQueue::model()->model()->get_pinned_posts();
    }

    private function get_news_details($link){


        $html = new SimpleHTMLDOM();
        $links = $html->file_get_html($link);
        $counter= 0 ;
        $data= array() ;
        if(empty($links))
            return false;

        /**Title  */
        $tags = 'meta[property=og:title]';
        $find = $links->find($tags);
        if(!empty($find))
            foreach ($find as $item)
                $data['title'] = $this->clearTag($item->content);
        /**Title  */



        /**Description*/
        $tags = 'meta[property=og:description]';
        $find = $links->find($tags);
        if(!empty($find))
            foreach ($find as $item)
                $data['description'] = $this->clearTag($item->content);
        /**Description*/



        /**Image Tag*/
        $tags = 'meta[property=og:image]';
        $find = $links->find($tags);
        if(!empty($find))
            foreach ($find as $item){
                $data['media']['image']['src'] = $item->content;
                $data['media']['image']['type'] = 'image';
                $counter++;
            }
        /**Image Tag*/



        /**Gallery  */
        $tags = 'div[class=textpic-box halfwidthheightratio]';
        if(!empty($links->find($tags))){
            $tags = 'div[class=textpic-box halfwidthheightratio] img';
            $find = $links->find($tags);
            foreach ($find as $item) {
                $data['media']['gallery']['src'][$counter] =  $item->src;
                $data['media']['gallery']['type'] =  'gallery';
                $data['media']['gallery']['caption'][$counter] =  $this->clearTag($item->parent()->parent()->last_child()->plaintext);
                $counter++;
            }
        }
        /**Gallery*/



        /**Image*/
        $tags = 'div[class=textpic-box] img';
        $find = $links->find($tags);
        if(!empty($find)){
            foreach ($find as $item) {
                $data['media']['gallery']['src'][$counter] =  $item->src;
                $data['media']['gallery']['type'] =  'gallery';
                $data['media']['gallery']['caption'][$counter] =   $this->clearTag($item->parent()->parent()->last_child()->plaintext);
                $counter++;
            }
        }
        /**Image*/



        /**Article body*/
        $tags = 'div[class=article-body-page]';
        $find = $links->find($tags);
        if(!empty($find))
            foreach ($find as $item)
                $data['body_description'] =  trim($item->plaintext);
        /**Article body*/



        /**Article info*/
        $tags = 'div[class=articleinfo] p[class=authname] span span';
        $find = $links->find($tags);
        if(!empty($find))
            foreach ($find as $item)
                $data['author']  =  trim($item->plaintext);
        /**Article info*/


        return $data;

    }


    protected function check_and_update_news($id,$post_id,$platform_id){

        if(!isset($this->all_news[$id])){
            return;
        }


        $this->platform_id=$platform_id;

        $this->post_id=$post_id;
        $this->news = clone $this->all_news[$id];


        $this->details=$this->get_news_details($this->news->link);
        if($this->news->title != $this->details['title'] || $this->news->description != $this->details['description']) {
            $this->category = Category::model()->findByPk($this->news->category_id);
            $this->news->title = $this->details['title'];

            $this->news->description = isset($this->details['description']) ? $this->details['description'] : null;
            $this->news->creator = isset($this->details['author']) ? $this->details['author'] : null;
            $this->news->schedule_date = $this->schedule_date;

            if ($this->news->save(true)) {
                $this->getMediaNews($this->news->id,$this->details['media']['image']);

                if (isset($this->details['media']['gallery']))
                    $this->getMediaNews($this->news->id, $this->details['media']['gallery']);

                $this->generate_post();
            }
        }


    }
    protected function getMediaNews($id, $data)
    {

        $criteria = new CDbCriteria;
        $criteria->compare('news_id',$id);
        MediaNews::model()->deleteAll($criteria);

        $media = new MediaNews();
        if($data['type'] == 'image'){


            $media->command=false;
            $media->id=null;
            $media->news_id = $id;
            $media->media = $data['src'];
            $media->type=$data['type'];
            $media->setIsNewRecord(true);
            if(!$media->save()){

            }


        }elseif($data['type'] == 'gallery'){


            foreach ($data['src'] as $item) {
                $empty_media = MediaNews::model()->findByAttributes(array('news_id'=> $id,'media'=>$item));
                if(empty($empty_media)){
                    $media->id=null;
                    $media->news_id = $id;
                    $media->media = $item;
                    $media->type=$data['type'];
                    $media->setIsNewRecord(true);
                    if(!$media->save()){

                    }
                }

            }
        }

    }

    private  function generate_post(){

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index=>$item) {
            $this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
            $this->hash_tag[$index]=' '.trim($item->title).' ';
        }

        $this->parent = null;



        if(isset($this->category->title))
            $this->category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_',$this->category->title));


        if(isset($this->sub_category->title))
            $this->sub_category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', isset($this->sub_category->title)));

        $this->source = Yii::app()->params['source'].' : ';

        $this->creator = false;


        $this->platform = Platform::model()->findByPk($this->platform_id);
        $this->post();

        $this->news->generated = 1;

        $this->news->save();
    }

    private function get_template($platform,$category){

        $temp  = PostTemplate::model()->findByAttributes(array(
            'platform_id'=>$platform->id,
            'catgory_id'=>$category,
        ));
        if(!empty($temp))
            return $temp;
        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition='( ( platform_id = '.$platform->id.' or platform_id is NUll ) and  catgory_id is NULL )';
        $temp  = PostTemplate::model()->find($cond);

        if(!empty($temp))
            return $temp;

        return Yii::app()->params['templates'];
    }

    protected function post(){

        $is_scheduled =1;

        $twitter_is_scheduled =false;

        $media= null;

        $temp = $this->get_template($this->platform,$this->news->category_id);

        $title = $this->news->title;

        $description = $this->shorten_point($this->news->description,'.');

        if($this->platform->title != 'Instagram'){
            $title = str_replace($this->hash_tag, $this->trim_hash, $this->news->title);
            $description = str_replace($this->hash_tag, $this->trim_hash, $this->shorten_point($this->news->description,'.'));
        }

        $title =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);
        $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

        $description =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);
        $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

        $shorten_url = $this->news->shorten_url;
        $full_creator = $this->source.$this->news->creator;
        if(!empty(strpos($this->news->creator,':')))
            $full_creator = $this->news->creator;

        $cre= $this->platform->title == 'Twitter'?($this->creator?$full_creator:null):$full_creator;


        $text = str_replace(array('[title]','[description]','[short_link]','[author]',),array($title,$description,$shorten_url,$cre),$temp['text']
        );

        if($this->platform->title == 'Facebook' or $this->platform->title == 'Twitter'){
            $text = str_replace('# ', '#', $text);
            $found = true;
            preg_match_all("/#([^\s]+)/", $text, $matches);
            if(isset($matches[0])){
                $matches[0] = array_reverse($matches[0]);
                $count = 0;
                foreach ($matches[0] as $hashtag){
                    if($count>=2)
                    {
                        $found = false;
                        $hashtag_without = str_replace('#','',$hashtag);
                        $hashtag_without = str_replace('_',' ',$hashtag_without);
                        $text = str_replace($hashtag,$hashtag_without,$text);

                    }
                    $count++;
                }

                if($count>=2) {
                    $found = false;
                }
            }

            if($found){
                $text = str_replace(array('[section]', '[sub_section]',), array($this->category, isset($this->sub_category->title)?$this->sub_category->title:null,), $text);
            }else{
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);
            }
        }elseif($this->platform->title=='Instagram'){
            $text = str_replace('# ', '#', $text);
            $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), $text);
        }


        if(!empty($this->model->image)){
            $newsMedia = MediaNews::model()->findByAttributes(array('media'=>$this->model->image));
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }
        }else{
            $newsMedia = MediaNews::model()->findAll('(type = "image" or type = "gallery") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $index_media= rand(0,count($newsMedia)-1);
                if(isset($newsMedia[$index_media])){
                    $media = $newsMedia[$index_media]->media;
                }
            }
        }
        if($this->platform->title == 'Twitter'){

            $newsMedia = MediaNews::model()->find('(type = "image") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }

            $text_twitter =$text;

            if($temp['type'] == 'Preview')
                if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                    $text =$text.PHP_EOL.$this->news->shorten_url;

            if($this->getTweetLength($text_twitter,$temp['type'] == 'Image'?true:false,$temp['type'] == 'Video'?true:false) > 141) {
                $is_scheduled = 0;
                $twitter_is_scheduled = true;
            }else{
                $twitter_is_scheduled =false;
                $text = $text_twitter.PHP_EOL;
                if($temp['type'] == 'Preview')
                    if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                        $text .=PHP_EOL.$this->news->shorten_url;
            }

        }else{
            if($twitter_is_scheduled){
                if(!$is_scheduled){
                    $is_scheduled = 1;
                }
            }
        }
        $text = str_replace('# #', '#', $text);
        $text = str_replace('##', '#', $text);
        $text = str_replace('&#8220;', '', $text);
        $text = str_replace('&#8221;', '', $text);
        $text = str_replace('&#8230;', '', $text);
        $text = str_replace('#8211;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&nbsp;', '', $text);
        $text = str_replace('&#160;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&ndash;', '', $text);
        if($this->platform->title != 'Instagram'){
            $text =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
            $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
        }

        $this->PostQueue = new PostQueue;
        $this->PostQueue->setIsNewRecord(true);
        $this->PostQueue->id= null;
        $this->PostQueue->type = $temp['type'];
        $this->PostQueue->post = $text;
        $this->PostQueue->schedule_date = $this->schedule_date;
        $this->PostQueue->catgory_id =  $this->news->category_id;
        $this->PostQueue->main_category_id =  $this->news->category_id;
        $this->PostQueue->link = $this->news->shorten_url;
        $this->PostQueue->is_posted = 0;
        $this->PostQueue->news_id = $this->news->id;
        $this->PostQueue->post_id =null;
        $this->PostQueue->media_url =$media;
        $this->PostQueue->settings ='general';
        $this->PostQueue->is_scheduled =$is_scheduled;
        $this->PostQueue->platform_id =$this->platform->id;
        $this->PostQueue->generated ='pinned';
        $this->PostQueue->created_at =date('Y-m-d H:i:s');
        $this->PostQueue->parent_id =null;

        $this->PostQueue->save();

        //$this->schedule_date = date('Y-m-d H:i:s', strtotime($this->schedule_date . ' + 1 minutes'));

    }


}