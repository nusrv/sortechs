<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $model_general Settings */
$this->pageTitle = "Settings";
$this->breadcrumbs=array(
	'Settings'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Programs', 'url'=>array('index')),
	array('label'=>'Create Programs', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#programs-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$(\"input[name='Settings[start_date_time]']\").attr('class','form-control');
$(\"input[name='Settings[end_date_time]']\").attr('class','form-control');
$(\"input[name='Settings[gap_time]']\").attr('class','form-control');
$(\"input[name='Settings[category_id]']\").attr('class','form-control');

$(\"select[name='Category[active]']\").attr('class','form-control');
");
?>
<script>
	$(document).ready(function(){
		if($('#Settings_direct_push').is(':checked')){
			$('.hiddenshowntimes').show();
		}else{
			$('.hiddenshowntimes').hide();

		}
		$('#Settings_direct_push').click(function(){
			if($('#Settings_direct_push').is(':checked')){
				$('.hiddenshowntimes').show();
			}else{
				$('.hiddenshowntimes').hide();

			}
		})

	})
</script>
<style>
	.hiddenshowntimes{
		display: none;
	}
</style>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header">


					<div class="row">
						<div class="col-sm-8">
							<div class="box-title ">
								<h3>General Settings</h3>
							</div>
						</div>
						<div class="col-sm-4" style="text-align: right"><?PHP //echo CHtml::link('Create',array('create'),array('class'=>'btn btn-flat btn-success','style'=>'margin-top: 14px;')) ?></div>

					</div>

				</div>
				<div class="box-body">
					<hr>

					<hr>
					<?php echo $this->renderPartial('_form',array('model'=>$model_general),true)?>
                    </div>
                </div>
            </div>


        <div class="box box-info">

        <div class="box-body">


        <h2>Custom settings</h2>
                    <?php echo $this->renderPartial('_form_settings',array('model'=>$model_create),true)?>

					<?php $this->widget('booster.widgets.TbGridView', array(
						'id' => 'settings-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'ajaxUpdate'=>false,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $model->search(true),
						'filter' => $model,
						'columns' => array(

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'category_id',
								'type'=>'raw',
								'value'=>'CHtml::link($data->cat->title,"#",array("data-value"=>$data->category_id,"rel"=>"Settings_category_id","data-pk"=>$data->id,"class"=>"editable editable-click"))',
								'sortable' => true,
								'editable' => array(
									'type' => 'select2',


									'url' => $this->createUrl('settings/edit'),
									'placement' => 'right',
									'inputclass' => 'span3',
									'source' => CHtml::listData(Category::model()->get_category(),'id','title')
								),
								'visible'=>$model->visible_f_name?true:false,
							),
							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'start_date_time',

								'sortable' => true,
								'editable' => array(
									'type' => 'time',
									'url' => $this->createUrl('settings/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_f_name?true:false,
							),

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'end_date_time',
								'sortable' => true,
								'editable' => array(
									'type' => 'time',
									'url' => $this->createUrl('settings/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_f_name?true:false,
							),

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'direct_push_start',
								'value'=>'$data->direct_push?$data->direct_push_start:"Not set"',
								'sortable' => true,
								'editable' => array(
									'type' => 'time',
									'url' => $this->createUrl('settings/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_f_name?true:false,
							),

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'value'=>'$data->direct_push?$data->direct_push_end:"Not set"',
								'name' => 'direct_push_end',
								'sortable' => true,
								'editable' => array(
									'type' => 'time',
									'url' => $this->createUrl('settings/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_f_name?true:false,
							),

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'direct_push',
								'type'=>'raw',
								'value'=>'CHtml::link($data->direct_push?"Yes":"No","#",array("data-value"=>$data->direct_push,"rel"=>"Settings_direct_push","data-pk"=>$data->id,"class"=>"editable editable-click"))',
								'sortable' => true,
								'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('settings/edit'),
									'placement' => 'right',
									'inputclass' => 'span3',
									'source' => array('0'=>'No',1=>'Yes')
								),
								'visible'=>$model->visible_f_name?true:false,
							),

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'gap_time',
								'sortable' => true,
								'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('settings/edit'),
									'placement' => 'right',
									'inputclass' => 'span3',
									'source' => $model->gap_time()
								),
								'visible'=>$model->visible_f_name?true:false,
							),






						/*	array('name' => 'category_id',
								'value'=>'$data->cat->title'
								),*/
							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{update}{delete}',
								'buttons' => array(
									'update' => array(
										'label'=>'Update Settings',
										'url'=>'Yii::app()->controller->createUrl("settings/update_settings",array("id"=>"$data->id"))',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-remove',
									)
								)
							),
						),
					)); ?>


				</div>

			</div>
		</div>

</section>
