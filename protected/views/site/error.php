<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<section class="content">
<div class="row">
	<div class="box box-info">
		<div class="box-header with-border">

	<div class="col-xs-12  alert alert-danger">
	<div class="col-xs-6  pull-left">
		<h2>Error <?php echo $code; ?></h2>
	</div>

	<div class="col-xs-12 ">
		<?php echo CHtml::encode($message); ?>
	</div>
</div>
			</div>
		</div>
	</div>
	</section>