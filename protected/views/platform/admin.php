<?php
/* @var $this PlatformController */
/* @var $model Platform */

$this->breadcrumbs=array(
	'Platforms'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Platform', 'url'=>array('index')),
	array('label'=>'Create Platform', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#platform-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-md-9"><h2>Platform</h2></div>
					<div class="col-md-3" style="padding-top: 19px;text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>

						<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Create', 'url'=>array('create')),
										)
									),
								),
								'htmlOptions'=>array(
									'class'=>'pull-right	'
								)
							)
						);

						?>
					</div>
				</div>
				<div class="box-body">
					<?PHP
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'platform-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $model->search(),
						'filter' => $model,
						'columns' => array(
							'id',
							'title',

							'created_at',
							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),
								)
							),
						)));?>

				</div>
			</div>
		</div>
</section>

