<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',

)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title'); ?>
	<?php //echo $form->textFieldGroup($model,'url'); ?>



 	<div class="col-sm-12">
<div class="col-sm-5">
	<div class="col-sm-2  col-sm-offset-7"  style="margin-top:10px;">
</div>
	<div class="col-sm-3  col-sm-pull-1">
	<?php //echo $form->checkboxGroup($model,'active'); ?>
		</div>
</div>
		</div>
	<div class="form-actions  pull-right" style="margin-bottom: 20px">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save'
			)
		); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
