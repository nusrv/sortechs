<?php
/* @var $form TbActiveForm */
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'form-visible',
)); ?>

    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'id',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_id',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_id?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'title',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_title',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                               // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>array_merge(array(''=>'Choose one'),CHtml::listData(Category::model()->findAll('deleted=0'),'title','title')),
                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_title?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'url',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_url',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                     'htmlOptions'=>array(

                        'disabled'=>$model->visible_url?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>

    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'active',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_active',
                    array(
                        'inline'=>true,
                         'widgetOptions'=>array(
                            'htmlOptions'=>array(
                             ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>array_merge(array(''=>'All status'),array('0'=>'Disabled','1'=>'Active')),
                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_active?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->datePickerGroup(
            $model,
            'created_at',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_created_at',
                    array(
                        'inline'=>true,
                         'widgetOptions'=>array(
                            'htmlOptions'=>array(
                             ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'options' => array(
                        'language' => 'en',
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy-mm-dd',
                    ),

                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_created_at?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2 pull-left page-sizes"  >

        <?php echo $form->dropDownListGroup(
            $model,
            'pagination_size',
            array(

                'widgetOptions'=>array(
                    'data'=>$model->pages_size(),
                    'htmlOptions'=>array(

                    ),
                ),
                'hint'=>''
            )
        ); ?>
</div>




<?php $this->endWidget(); ?>