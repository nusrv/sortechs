<?php
/* @var $this FillerDataController */
/* @var $model FillerData */

$this->pageTitle = "Filler data | Update";

$this->breadcrumbs=array(
	'Filler Data'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
?>

<script>

	window.App = {};
	$(window).load(function (){

		$(function(){
			var test = localStorage.input === 'true'? true: false;
			$('#calltoaction').prop('checked', test);
		});

		$('#calltoaction').on('change', function() {
			localStorage.input = $(this).is(':checked');
			console.log($(this).is(':checked'));
		});

		if($('#calltoaction').is(":checked")) {
			$( "#FillerData_type_call_to_action" ).prop( "disabled", false );
			$( "#FillerData_call_to_action" ).prop( "disabled", false );

		}else{
			$( "#FillerData_type_call_to_action" ).prop( "disabled", true );
			$( "#FillerData_call_to_action" ).prop( "disabled", true );

		}

		if($('#FillerData_platform_id option:selected').val() ==1){
			$('#all_call_to_action').show();

		}else{
			$('#all_call_to_action').hide();

		}


		$('#calltoaction').click(function(){
			if($('#calltoaction').is(":checked")) {
				$( "#FillerData_type_call_to_action" ).prop( "disabled", false );
				$( "#FillerData_call_to_action" ).prop( "disabled", false );

			}else{
				$( "#FillerData_type_call_to_action" ).prop( "disabled", true );
				$( "#FillerData_call_to_action" ).prop( "disabled", true );

			}
		});

		$platform = $('#FillerData_platform_id');
		$type = $('#FillerData_type');
		$group = $('#type_filler');
		$file_group = $('#file_filler');
		$youtube_group = $('#youtube_filler');
		$link_group = $('#link_filler');

		if($('#hidden-type').val() != null){
			$type.val($('#hidden-type').val().toLowerCase());
		}

		App.change=function(){
			if($platform.val() != ''){
				$type.find('option').remove().end();
				$.post('<?PHP echo CController::createUrl('/fillerData/getAll/') ?>',{id:$platform.val()}, function( data ) {
					if(data !='') {
						$group.show(); var toAppend = ''; data = jQuery.parseJSON( data);
						$.each(data,function(i,o){toAppend += '<option value="'+i+'">'+o+'</option>';});
						$type.append(toAppend);
					}
					if($type.val() == 'text'){
						$file_group.hide();
						$youtube_group.hide();
						$link_group.hide();
					}else if($type.val() == 'preview'){
						$link_group.show();
						$file_group.hide();
						$youtube_group.hide();
					}else if($type.val() == 'youtube'){
						$youtube_group.show();
						$file_group.hide();
						$link_group.hide();
					}else{
						if($type.val() == 'image')
							$('#change_label').text('Upload Image');
						else
							$('#change_label').text('Upload Video');
						$file_group.show();
						$youtube_group.hide();
						$link_group.hide();
					}
				});
			}else{
				$group.hide();
			}
		};
		$('#FillerData_platform_id').change(function(){
			if($('#FillerData_platform_id option:selected').val() ==1){
				$('#all_call_to_action').show();

			}else{
				$('#all_call_to_action').hide();

			}
		});

		if($type.val() == 'text'){
			$file_group.hide();
			$youtube_group.hide();
			$link_group.hide();
		}else if($type.val() == 'preview'){
			$link_group.show();
			$file_group.hide();
			$youtube_group.hide();
		}else if($type.val() == 'youtube'){
			$youtube_group.show();
			$file_group.hide();
			$link_group.hide();
		}else{
			if($type.val() == 'image')
				$('#change_label').text('Upload Image');
			else
				$('#change_label').text('Upload Video');
			$file_group.show();
			$youtube_group.hide();
			$link_group.hide();
		}

		$type.change(function(){
			if($type.val() == 'text'){
				$file_group.hide();
				$youtube_group.hide();
				$link_group.hide();
			}else if($type.val() == 'preview'){
				$link_group.show();
				$file_group.hide();
				$youtube_group.hide();
			}else if($type.val() == 'youtube'){
				$youtube_group.show();
				$file_group.hide();
				$link_group.hide();
			}else{
				if($type.val() == 'image')
					$('#change_label').text('Upload Image');
				else
					$('#change_label').text('Upload Video');
				$file_group.show();
				$youtube_group.hide();
				$link_group.hide();
			}
		});

		if($('#FillerData_platform_id').val() == 2 ){
			$type = $("#FillerData_type");
			$post = $("#FillerData_text");
			$("#twitter").show();
			var num=140;
			if($type.val()=='image' || $type.val()=='video' || $type.val()=='youtube'){
				num -=24;
			}
			if($type.val()=='preview'){
				num -=22;
			}
			num = num-$post.val().length;
			$("#twitter_counter").html(num);
		}else{
			$("#twitter").hide();
		}

		$('#FillerData_platform_id').change(function(){
			if($(this).val() == 2){
				$('#twitter').show();

			}else{
				$('#twitter').hide();
			}
		});

		$('#FillerData_text').keyup(function(e){


			$platform = $('#FillerData_platform_id');

			$type = $("#FillerData_type");
			$post = $("#FillerData_text");
			var num=140;
			var status = false;
			$platform.each(function(i){
				if($(this).val() == 2 ){

					status = true;

				}
			});

			if(status){
				if($type.val()=='image' || $type.val()=='video' || $type.val()=='youtube'){
					num -=24;
				}
				if($type.val()=='preview'){
					num -=22;
				}
				var input =num -$(this).val().length;
				if(input >0){
					$("#twitter_counter").text(input);
					$("#yw1").show();
				}else{
					$("#twitter_counter").text(input);
					$("#yw1").hide();
				}
			}
		});

		$('#FillerData_type').on('change',function(){
			$platform = $('#FillerData_platform_id');
			$type = $("#FillerData_type");
			$post = $("#FillerData_text");



			$platform.each(function(i){
				if($(this).val() == 2 ){
					$("#twitter").show();
					var num=140;
					if($type.val()=='image' || $type.val()=='video' || $type.val()=='youtube'){
						num -=24;
					}
					if($type.val()=='preview'){
						num -=22;
					}
					num = num-$post.val().length;
					$("#twitter_counter").html(num);
				}else{
					$("#twitter").hide();
				}
			});
		});
		$('#EnglishDirection').click(function(){
			if($('#PostDir').hasClass('arabic-direction')){
				$('#PostDir').removeClass('arabic-direction');
			}
		})
		$('#ArabicDirections').click(function(){
			if(!$('#PostDir').hasClass('arabic-direction')){
				$('#PostDir').addClass('arabic-direction');
			}
		})

	});
</script>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Create', 'url'=>array('create')),
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
								),
								/*'htmlOptions'=>array(
									'class'=>'pull-right	'
								)*/
							)
						);

						?></div>
					<div class="col-sm-3" style="text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>




					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
</section>
