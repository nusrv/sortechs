<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $ModelGeneratorPost GeneratorPost */
/* @var $type_post GeneratorPost */

$this->pageTitle = "Post queue | Create";

$this->breadcrumbs = array(
    'Post Queues' => array('main'),
    'Create',
);

Yii::app()->clientScript->registerScript('App', '

');
?>

<script>

    window.App = {};
    $(window).load(function () {

        $( "form" ).submit(function() {
            $('.form-actions ').hide();



        });

        $(function () {
            var test = localStorage.input === 'true' ? true : false;
            $('#calltoaction').prop('checked', test);
        });

        $('#calltoaction').on('change', function () {
            localStorage.input = $(this).is(':checked');
            console.log($(this).is(':checked'));
        });

        if ($('#calltoaction').is(":checked")) {
            $("#PostQueue_type_call_to_action").prop("disabled", false);
            $("#PostQueue_call_to_action").prop("disabled", false);

        } else {
            $("#PostQueue_type_call_to_action").prop("disabled", true);
            $("#PostQueue_call_to_action").prop("disabled", true);

        }

        if ($('#PostQueue_platform_id_0').is(":checked")) {
            $('#all_call_to_action').show();

        } else {
            $('#all_call_to_action').hide();

        }


        $('#calltoaction').click(function () {
            if ($('#calltoaction').is(":checked")) {
                $("#PostQueue_type_call_to_action").prop("disabled", false);
                $("#PostQueue_call_to_action").prop("disabled", false);

            } else {
                $("#PostQueue_type_call_to_action").prop("disabled", true);
                $("#PostQueue_call_to_action").prop("disabled", true);

            }
        });

        if ($('#PostQueue_platform_id_1').is(":checked")) {
            $('#twitter').show();

            $typed = $("#PostQueue_type");
            $postd = $("#PostQueue_post");
            var num = 140;


            if ($typed.val() == 'image' || $typed.val() == 'video' || $typed.val() == 'youtube') {
                num -= 24;
            }
            if ($typed.val() == 'preview') {
                num -= 22;
            }
            var input = num - $postd.val().length;
            if (input > 0) {
                $("#twitter_counter").text(input);
                $("#yw0").show();
            } else {
                $("#twitter_counter").text(input);
                $("#yw0").hide();
            }


        } else {
            $('#twitter').hide();
        }

        $type = $('#PostQueue_type');
        $group = $('#type_PostQueue');
        $file_group = $('#file_PostQueue');
        $youtube_group = $('#youtube_PostQueue');
        $link_group = $('#link_PostQueue');

        /*App.change=function() {*/
        $('.Platforms').click(function () {



            /*};*/
            if ($('.Platforms').val() != '') {

                if ($("#PostQueue_platform_id_2").is(":checked")) {
                    $platform = 3;
                } else if (!$("#PostQueue_platform_id_2").is(":checked")) {
                    if ($('#PostQueue_platform_id_1').is(":checked")) {
                        $platform = 2;

                    } else {
                        $platform = 1;
                    }
                }


                $.post('<?PHP echo CController::createUrl('/postQueue/getAll/') ?>', {id: $platform}, function (data) {

                    if (data != '') {
                        $group.show();
                        var toAppend = '';
                        var type_value = $("#PostQueue_type option:selected").val();
                        data = jQuery.parseJSON(data);
                        $.each(data, function (i, o) {
                            toAppend += '<option value="' + i + '">' + o + '</option>';

                        });
                        $type.html(toAppend);
                        $('#PostQueue_type option[value='+type_value+']').attr('selected','selected');

                    }
                    if ($('#PostQueue_platform_id_0').is(":checked")) {
                        $('#all_call_to_action').show();

                    } else {
                        $('#all_call_to_action').hide();

                    }
                    if ($type.val() == 'text') {
                        $file_group.hide();
                        $youtube_group.hide();
                        $link_group.hide();
                    } else if ($type.val() == 'preview') {
                        $link_group.show();
                        $file_group.show();
                        $youtube_group.hide();
                    } else if ($type.val() == 'youtube') {
                        $youtube_group.show();
                        $file_group.hide();
                        $link_group.hide();
                    } else {
                        if ($type.val() == 'image')
                            $('#change_label').text('Upload Image');
                        else
                            $('#change_label').text('Upload Video');
                        $file_group.show();
                        $youtube_group.hide();
                        $link_group.hide();
                    }
                });
            } else {
                $group.hide();
            }
        });


        if ($type.val() == 'text') {
            $file_group.hide();
            $youtube_group.hide();
            $link_group.hide();
        } else if ($type.val() == 'preview') {
            $link_group.show();
            $file_group.show();
            $youtube_group.hide();
        } else if ($type.val() == 'youtube') {
            $youtube_group.show();
            $file_group.hide();
            $link_group.hide();
        } else {
            if ($type.val() == 'image')
                $('#change_label').text('Upload Image');
            else
                $('#change_label').text('Upload Video');
            $file_group.show();
            $youtube_group.hide();
            $link_group.hide();
        }

        $type.change(function () {
            if ($type.val() == 'text') {
                $file_group.hide();
                $youtube_group.hide();
                $link_group.hide();
            } else if ($type.val() == 'preview') {
                $link_group.show();
                $file_group.show();
                $youtube_group.hide();
            } else if ($type.val() == 'youtube') {
                $youtube_group.show();
                $file_group.hide();
                $link_group.hide();
            } else {
                if ($type.val() == 'image')
                    $('#change_label').text('Upload Image');
                else
                    $('#change_label').text('Upload Video');
                $file_group.show();
                $youtube_group.hide();
                $link_group.hide();
            }


        });


        $('.Platforms').change(function () {
            if ($('#PostQueue_platform_id_1').is(":checked")) {
                $('#twitter').show();

            } else {
                $('#twitter').hide();
            }
        });

        $('#PostQueue_post').keyup(function (e) {


            $platform = $('.Platforms');

            $type = $("#PostQueue_type");
            $post = $("#PostQueue_post");
            var num = 140;
            var status = false;
            $platform.each(function (i) {
                if ($('#PostQueue_platform_id_1').is(":checked")) {

                    status = true;

                }
            });

            if (status) {
                if ($type.val() == 'image' || $type.val() == 'video' || $type.val() == 'youtube') {
                    num -= 24;
                }
                if ($type.val() == 'preview') {
                    num -= 22;
                }
                var input = num - $(this).val().length;
                if (input > 0) {
                    $("#twitter_counter").text(input);
                    $("#yw1").show();
                } else {
                    $("#twitter_counter").text(input);
                    $("#yw1").hide();
                }
            }
        });

        $('#PostQueue_type').on('change', function () {
            $platform = $('.Platforms');
            $type = $("#PostQueue_type");
            $post = $("#PostQueue_post");


            $platform.each(function (i) {
                if ($('#PostQueue_platform_id_1').is(":checked")) {
                    $("#twitter").show();
                    var num = 140;
                    if ($type.val() == 'image' || $type.val() == 'video' || $type.val() == 'youtube') {
                        num -= 24;
                    }
                    if ($type.val() == 'preview') {
                        num -= 22;
                    }
                    num = num - $post.val().length;
                    $("#twitter_counter").html(num);
                } else {
                    $("#twitter").hide();
                }
            });
        });
        $platform = $('.Platforms');
        $enD = $('#EnglishDirection');
        $arD = $('#ArabicDirections');
        $PostDir = $('#PostDir');

        $enD.click(function () {
            if ($PostDir.hasClass('arabic-direction')) {
                $PostDir.removeClass('arabic-direction');
            }
        });

        $arD.click(function () {
            if (!$PostDir.hasClass('arabic-direction')) {
                $PostDir.addClass('arabic-direction');
            }
        });
        $platform.each(function (i) {
            if ($('#PostQueue_platform_id_1').is(":checked")) {
                $("#twitter").show();
                var num = 140;
                if ($type.val() == 'image' || $type.val() == 'video' || $type.val() == 'youtube') {
                    num -= 24;
                }
                if ($type.val() == 'preview') {
                    num -= 22;
                }
                num = num - $post.val().length;
                $("#twitter_counter").html(num);
            } else {
                $("#twitter").hide();
            }
        });
        $('#GeneratorPost_link').on('paste', function () {
            setTimeout(function () {
                $.post('<?PHP echo CController::createUrl('/postQueue/showDetailsUrl/') ?>', {link: $('#GeneratorPost_link').val()}, function (data) {

                    $('#link-img').html(data);
                });
            }, 10);
        });

        $('#GeneratorPost_link').change(function () {
            $.post('<?PHP echo CController::createUrl('/postQueue/showDetailsUrl/') ?>', {link: $('#GeneratorPost_link').val()}, function (data) {
                $('#link-img').html(data);
            });
        });


		if($('#GeneratorPost_link').val() != ''){
			$.post('<?PHP echo CController::createUrl('/postQueue/showDetailsUrl/') ?>', {link: $('#GeneratorPost_link').val()}, function (data) {
				$('#link-img').html(data);
			});
		}
      /*  var arabic = /[\u0600-\u06FF]/;
        var english ="\u00e6\u00f8\u00e5";

        arabic.test(string);*/
        $('#PostQueue_post').change(function() {

            $.post('<?PHP echo CController::createUrl('/postQueue/getHashtags/') ?>', {post: $(this).val()}, function (data) {
                $('#PostQueue_post').val(data);
            });
        });


        function getCaretPositions(ctrl) {
            var start, end;
            if (ctrl.setSelectionRange) {
                start = ctrl.selectionStart;
                end = ctrl.selectionEnd;
            } else if (document.selection && document.selection.createRange) {
                var range = document.selection.createRange();
                start = 0 - range.duplicate().moveStart('character', -100000);
                end = start + range.text.length;
            }
            return {
                start: start,
                end: end
            }
        }


        $('#checked_hashtags').change(function(){
            if($('#checked_hashtags').is(":checked")) {
                $(this).parent().removeClass('btn-danger');
                $(this).parent().addClass('btn-success');
            }else{
                $(this).parent().removeClass('btn-success');
                $(this).parent().addClass('btn-danger');

            }

                function getSelectionText() {
                    var text = "";
                    if (window.getSelection) {
                        text = window.getSelection().toString();
                    } else if (document.selection && document.selection.type != "Control") {
                        text = document.selection.createRange().text;
                    }
                    return text;
                }
                function replaceAll(str, find, replace) {
                    return str.replace(new RegExp(find, 'g'), replace);
                }
                var textarea = document.querySelector("textarea");
                function getSurroundingSelection(textarea) {
                    return [textarea.value.substring(0, textarea.selectionStart)
                        , textarea.value.substring(textarea.selectionStart, textarea.selectionEnd)
                        , textarea.value.substring(textarea.selectionEnd, textarea.value.length)]
                }
                $('#PostQueue_post').click(function (e) {
                    if($('#checked_hashtags').is(":checked")) {
                        var caret = getCaretPositions(this);
                        var result = /\S+$/.exec(this.value.slice(0, caret.end));
                        var lastWord = result ? result[0] : null;
                        var pos = getSurroundingSelection(textarea);
                        var replaced = "";
                        var replaces = "";
                        var dash = true;
                        var chars = "";
                        var x=0;
                        var return_hashtag = null;
                        var z = getSelectionText();
                        var vc = $(this).val();
                        var arr = vc.split(" ");
                        var splitted_z = z.split(" ");
                            $.each(splitted_z, function (z_key, z_val) {
                                    var count_items = splitted_z.length;
                                    $.each(arr, function (key, val) {
                                        if (val.indexOf(z_val) > -1) {

                                            if (count_items > 1) {
                                                if(splitted_z[0].length >=2) {
                                                    val = val.trim();
                                                    if (val.indexOf(" ") == -1) {
                                                        if (dash < 3) {
                                                            replaced += val;
                                                            replaces += val;
                                                            replaced += '_';
                                                            replaces += ' ';
                                                            dash++;
                                                        }
                                                    }
                                                }
                                            } else {
                                                replaced = val;
                                            }
                                            if (lastWord.indexOf('#') != -1) {

                                                return_hashtag = lastWord.split('#').join('').split('_').join(' ');

                                            }
                                        }
                                    });
                            });
                            if (lastWord.indexOf('#') == -1) {
                                if (replaced != 0) {

                                    if (replaces == 0) {
                                        /*
                                         $('#PostQueue_post').val(replaceAll($('#PostQueue_post').val(),replaced,"#"+replaced));
                                         */
                                        pos[1].trim();

                                        if(pos[1] != lastWord) {
                                            for (var i = pos[1].length; i < lastWord.length ;i++) {
                                                chars += lastWord.charAt(x);
                                                pos[0] = pos[0].substring(0, pos[0].length-1);
                                                x++;
                                            }
                                            pos[1] = chars +pos[1];
                                        }
                                        pos[1] = pos[1].replace(lastWord,"#"+lastWord);
                                        $('#PostQueue_post').val(pos[0] + pos[1] + pos[2]);
                                    } else {
                                        var replaces_items = replaces.split(" ");
                                        var uniqueNames = [];
                                        $.each(replaces_items, function(i, el){
                                            if($.inArray(el, uniqueNames) === -1){
                                                if(el.indexOf('_') == -1)
                                                uniqueNames.push(el);
                                            }
                                        });
                                        var replaced_final = uniqueNames[0] +" " +lastWord;
                                        var rep_final = replaced_final.split(" ").join("_");
                                        if(pos[1] != replaced_final) {
                                            for (var i = pos[1].length; i < replaced_final.length ;i++) {
                                                chars += replaced_final.charAt(x);
                                                pos[0] = pos[0].substring(0, pos[0].length-1);
                                                x++;
                                            }
                                            pos[1] = chars +pos[1];
                                        }
                                        pos[1] = pos[1].replace(replaced_final,"#"+rep_final);
                                        $('#PostQueue_post').val(pos[0] + pos[1] + pos[2]);
                                    }
                                }
                            } else {

                                if(pos[1].length > lastWord.length) {
                                    for (var i = lastWord.length; i < pos[1].length ;i++) {
                                        chars += pos[1].charAt(x);

                                        x++;
                                    }
                                    pos[0] = pos[0]+chars;
                                }else{
                                    for (var i = pos[1].length; i < lastWord.length ;i++) {
                                        chars += lastWord.charAt(x);
                                         pos[0] = pos[0].substring(0, pos[0].length-1);
                                        x++;
                                    }
                                    pos[1] = chars + pos[1];
                                }
                                if (return_hashtag != null) {
                                    pos[1] = return_hashtag;
                                }
                                $('#PostQueue_post').val(pos[0] + pos[1] + pos[2]);
                            }
                        }

                });

        });


    });


    App.select_image = function ($src) {

        if($('#ytGeneratorPost_image').val() != ''){
            $('#'+$("img[src='"+$('#ytGeneratorPost_image').val()+"']").attr('id')).removeClass('highlight');
            $('#'+$("img[src='"+$('#ytGeneratorPost_image').val()+"']").attr('id')).addClass('grayscale');
        }
        $('#'+$("img[src='"+$src+"']").attr('id')).addClass('highlight');
        $('#'+$("img[src='"+$src+"']").attr('id')).removeClass('grayscale');
        $('#ytGeneratorPost_image').val($src);
    };


</script>
<!--<script type="text/javascript">
	$body = $("body");

	$(document).on({
		ajaxStart: function() { $body.addClass("loading modal");    },
		ajaxStop: function() { $body.removeClass("loading modal"); }
	});
</script>-->

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-sm-12 pull-right">
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>
                    </div>

                </div>
                <div class="box-body">
                    <?php echo TbHtml::button('Non thematic',array('id'=>'btn_12','class'=>'btn btn-active'))?>
                    <?php   TbHtml::link('Thematic',array('/thematic_post'),array('class'=>'btn btn-link'))?>

                        <?php
                        echo $this->renderPartial('_form', array('model' => $model), true);
                        ?>

                    <?php //$this->renderPartial('_form', array('model'=>$model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
