<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $form TbActiveForm */
$form = $this->beginWidget('booster.widgets.TbActiveForm',
    array(
        'id' => 'search-form',
        'type' => 'horizontal',
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )
);
?>
<style>
    span.required {
        display: none;
    }

</style>
<?php
if(Yii::app()->controller->action->id == 'main'){
    $primry_label = 'Post queue';
    $primry_action = $this->createUrl('main');
}elseif(Yii::app()->controller->action->id == 'mainUn'){
    $primry_label = 'UnScheduled ';
    $primry_action = $this->createUrl('mainUn');
}elseif(Yii::app()->controller->action->id == 'mainPosted'){
    $primry_label = 'Pushed posts';
    $primry_action = $this->createUrl('mainPosted');

}elseif(Yii::app()->controller->action->id == 'mainPinned'){
    $primry_label = 'Pinned posts';
    $primry_action = $this->createUrl('mainPinned');

}
?>
 <div class="row">

    <div class="col-sm-3">
        <a href="#" class="col-sm-2" style="margin-top:10px;" onclick="guiders.show('second');return false"    ><i class="fa fa-question-circle"></i></a>

             <?PHP echo $form->dropDownListGroup($model, 'type', array(
            'widgetOptions' => array(
                'data' => Yii::app()->params['rule_filter'],
                'htmlOptions' => array(
                    'class' => 'input-sm '
                )
            ),
            'labelOptions' => array(
                'class' => 'col-sm-4 col-sm-pull-1'
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5 input-sm col-sm-pull-3',
            ),


        )); ?>
     </div>

    <div class="col-sm-3">
        <a href="#" style="margin-top:12px;"   class="col-sm-2 col-sm-pull-5"  onclick="guiders.show('third');return false"    ><i class="fa fa-question-circle"></i></a>

        <?PHP
        $data =array(''=>'--select--')+CHtml::listData(Category::model()->findAll('deleted=0'), 'id', 'title');

        echo $form->dropDownListGroup($model, 'catgory_id',
            array(

                'widgetOptions'=>array(
                    'data' => $data,
                    'htmlOptions' => array(
                        'class' => 'input-sm'
                    ),
                    'options' => array(
                        $model->catgory_id => array('selected' => true),
                    ),
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-4 col-sm-pull-5',
                    'style'=>'margin-left:-6px;margin-top:5px;'
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5 input-sm col-sm-pull-6',
                ),
            ));
        ?></div>


    <div class="col-sm-2 ">


        <?PHP
        echo $form->datePickerGroup(
            $model,
            'schedule_date',
            array(
                'widgetOptions' => array(
                    'options' => array(
                        'viewformat' => 'yyyy-mm-dd',
                        'format' => 'yyyy-mm-dd',
                    ),
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-3 col-sm-pull-9'
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-9 col-sm-pull-9',
                    'style' => 'padding-left: 14px;'
                ),
            )
        );


        ?></div>
    <div class="col-sm-2">

        <?PHP
        echo $form->datePickerGroup(
            $model,
            'to',
            array(
                'widgetOptions' => array(
                    'options' => array(
                        'viewformat' => 'yyyy-mm-dd',
                        'format' => 'yyyy-mm-dd',
                    )
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-3 col-sm-pull-9'
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-9 col-sm-pull-9',
                    'style' => 'padding-left: 14px;'
                ),
            )
        );
        ?></div>
    <div class="col-sm-2 col-sm-pull-1">

        <?php
         $this->widget(
            'booster.widgets.TbButtonGroup',
            array(
                'context' => 'success',
                // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'buttons' => array(
                    array('label' => $primry_label,'buttonType'=>'link' ,'url' => $primry_action),
                    array(
                        'items' => array(
                            array('label' => 'Post queue', 'url' => $this->createUrl('main')),
                            array('label' => 'UnScheduled posts', 'url' => $this->createUrl('mainUn')),
                             '---',
                            array('label' => 'Pushed posts', 'url' => $this->createUrl('mainPosted')),
                           // array('label' => 'Pinned posts', 'url' => $this->createUrl('mainPinned')),
                        )
                    ),
                ),
            )
        ); echo ' ';
        ?>
    <!--           <?PHP
/*        if(Yii::app()->controller->action->id == 'main')
        echo CHtml::link('Go to UnScheduled Posts',$this->createUrl('mainUn'),array('class'=>'btn btn-primary btn-sm btn-top','id'=>'gotoposts','style'=>'margin-top:2px;'));
        else
            echo CHtml::link('Go to Post Queue',$this->createUrl('main'),array('class'=>'btn btn-success btn-sm btn-top','id'=>'gotoposts','style'=>'margin-top:2px;'));
        */?>
        --><?php
/*        echo CHtml::link('Pushed posts',$this->createUrl('mainPosted'),array('class'=>'btn btn-info btn-sm btn-top','style'=>'margin-top:2px;'))
        */?>

    </div>
    <!-- <div class="col-sm-2">

     </div>-->
</div>
<div class="row">

    <div class="col-sm-5 ">
        <a href="#"     class="col-sm-2 " style="margin-top:8px;" onclick="guiders.show('seven');return false"    ><i class="fa fa-question-circle"></i></a>

        <?PHP
        echo $form->select2Group(
            $model,
            'platform',
            array(
                'widgetOptions' => array(
                    'asDropDownList' => false,
                    'options' => array(
                         'tags' => array('Facebook', 'Instagram', 'Twitter'),
                        //'tags' => array('Instagram', 'Twitter'),
                        'placeholder' => 'type clever, or is, or just type!',
                        'tokenSeparators' => array(',', ' ')
                    )
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-2 col-sm-pull-1'
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-7  input-sm col-sm-pull-1',

                ),
            )
        ); ?>
    </div>
    <div class="col-sm-3 ">

        <?PHP
        echo $form->dropDownListGroup(
            $model,
            'sorts',
            array(
                'widgetOptions' => array(
                    'data'=>array('schedule_date'=>'Date','type'=>'Type','catgory_id'=>'Sections','newest'=>'Newest','id'=>'Oldest')
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-6  col-sm-pull-2',
                    'style'=>'margin-top:5px;',
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-6 input-sm   col-sm-pull-5',
                ),
            )
        ); ?>
    </div>
    <div class="col-sm-3 ">

        <?PHP
        echo $form->dropDownListGroup(
            $model,
            'view_posts',
            array(
                'widgetOptions' => array(
                    'data'=>array('all_posts'=>'All posts','Emirate_al_youm'=>'Automated posts','created_by'=>'One time posts')
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-4 col-sm-pull-4',
                    'style'=>'margin-top:5px;',
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-6 input-sm  col-sm-pull-6',
                ),
            )
        ); ?>

    </div>
</div>
<!-- <div class="col-sm-12">
        <br>
    <?PHP /*if(Yii::app()->controller->id == 'site' and Yii::app()->controller->action->id == 'index'){ */ ?>
     <?PHP /*echo CHtml::link('Reset','javascript:App.reset("queue")',array('class'=>'btn btn-primary queue','data-url'=>CController::createUrl('site/resetFilter'))) */ ?>
        <?PHP /*echo CHtml::link('Clear Queue',array('site/clear_queue'),array('class'=>'btn btn-warning'))*/ ?>
    <?PHP /*} */ ?>
    <?PHP /*if(Yii::app()->controller->id == 'site' and Yii::app()->controller->action->id == 'postUnSchedule'){ */ ?>
     <?PHP /*echo CHtml::link('Reset','javascript:App.reset("unscheduled")',array('class'=>'btn btn-primary unscheduled','data-url'=>CController::createUrl('site/resetFilter'))) */ ?>
    <?PHP /*} */ ?>

    </div>
</div>-->
<?PHP $this->endWidget(); ?>



<!--Tour-->

<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'second',
        'title'        => 'Type',
        'next'         => 'third',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Search by the type(image,video,preview or text) or  leave it blank if you want to get all types',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#PostQueue_type',
        'position'      => 7,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'third',
        'title'        => 'Category',
        'next'         => 'forth',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Search by posts categories or choose --select-- to get all categories',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#PostQueue_catgory_id',
        'position'      => 5,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'forth',
        'title'        => 'From Date',
        'next'         => 'fifth',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'show posts from this date',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#PostQueue_schedule_date',
        'position'      => 5,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'fifth',
        'title'        => 'To Date',
        'next'         => 'six',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'show posts to this date',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#PostQueue_to',
        'position'      => 5,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
if(Yii::app()->controller->action->id == 'main'){
?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'six',
        'title'        => 'UnScheduled Posts',
        'next'         => 'seven',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Show Posts that is Unscheduled to be posted',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#gotoposts',
        'position'      => 5,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php }else{ ?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'six',
        'title'        => 'Posts',
        'next'         => 'seven',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'View the posts that are scheduled to be posted',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#gotoposts',
        'position'      => 5,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php } ?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'seven',
        'title'        => 'Platforms',
        'next'         => 'showbtn',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Search by platforms',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#s2id_autogen1',
        'position'      => 5,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'showbtn',
        'title'        => 'Show button',
        'next'         => 'editbtn',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('seven');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array(
                'name'   => 'Skip search post tour',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('eight');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Show this items all properties',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '.showbtn',
        'position'      => 11,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'editbtn',
        'title'        => 'Edit button',
        'next'         => 'unScheduledbtn',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('showbtn');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array(
                'name'   => 'Skip search post tour',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('eight');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Edit this items all properties',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '.editbtn',
        'position'      => 11,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php if(Yii::app()->controller->action->id == 'main') { ?>
    <?php
    $this->widget('ext.eguiders.EGuider', array(
            'id' => 'unScheduledbtn',
            'title' => 'UnScheduled button',
            'next' => 'eight',
            'buttons' => array(
                array(
                    'name' => 'Previous',
                    'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('editbtn');}"
                ),
                array(
                    'name' => 'Next',

                ),
                array(
                    'name' => 'Skip search post tour',
                    'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('eight');}",
                ),
                array(
                    'name' => 'Exit',
                    'onclick' => "js:function(){guiders.hideAll();}"
                )
            ),
            // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
            'description' => 'UnSchedule  items ',
            'overlay' => true,
            // you can attach your guide to any element in the page thanks to JQuery selectors
            'attachTo' => '.unScheduledbtn',
            'position' => 11,
            'xButton' => true,
            'onShow' => 'js:function(){ $(".highlight pre").show();}',
            'closeOnEscape' => true,
        )
    );
}else{
    $this->widget('ext.eguiders.EGuider', array(
            'id' => 'unScheduledbtn',
            'title' => 'Post button',
            'next' => 'eight',
            'buttons' => array(
                array(
                    'name' => 'Previous',
                    'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('editbtn');}"
                ),
                array(
                    'name' => 'Next',

                ),
                array(
                    'name' => 'Skip search post tour',
                    'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('eight');}",
                ),
                array(
                    'name' => 'Exit',
                    'onclick' => "js:function(){guiders.hideAll();}"
                )
            ),
            // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
            'description' => 'Add items to be Posted',
            'overlay' => true,
            // you can attach your guide to any element in the page thanks to JQuery selectors
            'attachTo' => '.unScheduledbtn',
            'position' => 11,
            'xButton' => true,
            'onShow' => 'js:function(){ $(".highlight pre").show();}',
            'closeOnEscape' => true,
        )
    );
}
?>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'eight',
        'title'        => 'Post Queue',
        'next'         => 'nine',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('unScheduledbtn');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'All Posts in the queue',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#PostQueueController',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$createLink = Yii::app()->createUrl('postQueue/create',array('#' => 'guider=createpostfirst'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'nine',
        'title'        => 'Create one time post',
        'next'         => 'ten',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('eight');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$createLink';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'to create a new post that is not generated from the site',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#onetimepost',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$manageposttemplate = Yii::app()->createUrl('postTemplate/admin',array('#' => 'guider=managefirst'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'ten',
        'title'        => 'Post Templates',
        'next'         => 'eleven',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('nine');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$manageposttemplate';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'To construct template assigns how your post is going to be posted',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#posttemplates',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>

<?php
$sections = Yii::app()->createUrl('category/admin',array('#' => 'guider=firstSection'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'eleven',
        'title'        => 'Sections',
        'next'         => 'twelve',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('ten');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$sections';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Update and remove and view all the sections',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#sectionsItems',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$sections = Yii::app()->createUrl('subCategories/admin',array('#' => 'guider=firstSection'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'twelve',
        'title'        => 'Sub sections',
        'next'         => 'thirteen',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('eleven');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$sections';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Update and remove and view all the sub sections',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#subsectionsItems',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$sections = Yii::app()->createUrl('fillerData/admin',array('#' => 'guider=firstFiller'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'thirteen',
        'title'        => 'Filler Data',
        'next'         => 'fourteen',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('twelve');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$sections';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Construct posts that are filled daily based on your scheduled day and time',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#FillerDataitem',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$sections = Yii::app()->createUrl('hashtag/admin',array('#' => 'guider=firstHashtag'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'fourteen',
        'title'        => 'Hashtag',
        'next'         => 'fifteen',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('thirteen');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$sections';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Manage Hashtags',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#HashtagManager',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$sections = Yii::app()->createUrl('coverPhoto/admin',array('#' => 'guider=firstCover'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'fifteen',
        'title'        => 'Cover photo',
        'next'         => 'sixteen',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourteen');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$sections';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Manage cover photos',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#CoverManager',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$sections = Yii::app()->createUrl('settings/update/1',array('#' => 'guider=firstSettings'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'sixteen',
        'title'        => 'Settings',
        'next'         => 'seventeen',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifteen');}"
            ),
            array(
                'name'   => 'Next',

            ),
            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$sections';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Update post Settings',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#Settingstour',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$sections = Yii::app()->createUrl('pdf',array('#' => 'guider=firstPdf'));
 $this->widget('ext.eguiders.EGuider', array(
        'id'           => 'seventeen',
        'title'        => 'PDF',
        'next'         => 'end',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('sixteen');}"
            ),

            array('name'=>'Tour','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$sections';}"),

            array(
                'name'   => 'Back to search Posts',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}",
            ),
            array(
                'name'   => 'End',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Download PDF for desired dates',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#PDFtour',
        'position'      => 3,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
 $Previous = Yii::app()->createUrl('settings/update/1',array('#' => 'guider=SettingsTour'));
$continueTour = Yii::app()->createUrl('pdf',array('#' => 'guider=PDFTour'));
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'postQueueTour',
        'title'        => 'Post Queue',
        'next' =>'EditPostsTour',
        'buttons'      => array(
            array('name'=>'Previous ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$Previous';}"),

            array('name'=>'Next ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),



            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

        'description'   => 'Sortechs will pull content from your website, and automatically generate posts tailored to fit each of your channels. It will also automatically assign dates and times and then show them to you in this review panel. ',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#PostQueueController',
        'position'      => 3,
        'xButton'       => true,
         'autoFocus'      => true,
        'closeOnEscape' => true,
    )
);
?>

<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=postQueueTour'));
$Previous = Yii::app()->createUrl('settings/update/1',array('#' => 'guider=SettingsTour'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'EditPostsTour',
        'title'        => 'Edit Posts',

        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('postQueueTour');}"
            ),
            array('name'=>'Next ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),



            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

        'description'   => 'Here you can review and edit the posts as freely as you would like. You can edit text and media, delete, approve, and push posts in a few simple steps. ',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '.fa-eye',
        'position'      => 6,
        'xButton'       => true,
        'autoFocus'      => true,
        'closeOnEscape' => true,
    )
);
?>

<?php
 $Previous = Yii::app()->createUrl('pdf',array('#' => 'guider=PDFTour'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'FinalStepTour',
        'title'        => 'Extra features',

        'buttons'      => array(

            array('name'=>'Previous ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$Previous';}"),



            array(
                'name'   => 'End',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

        'description'   => 'Check out our extra features here! You can update hashtag directories, schedule cover photos and profile pictures, automatically assign and post image galleries, sliders, carousel, photo albums and much more. ',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#onetimepost',
        'position'      => 4,
        'show'=>false,
        'xButton'       => true,
        'autoFocus'      => true,
        'closeOnEscape' => true,
    )
);
?>
<!--endTour-->