<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $url_rss

 * @property integer $deleted
 * @property integer $active

 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property News[] $news
 * @property PostQueue[] $postQueues
 * @property SubCategories[] $Subcategory
 */
class Category extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title,created_at', 'required'),
			array('deleted, active', 'numerical', 'integerOnly'=>true),
			array('title, url', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title,url_rss, url, deleted, active, created_at,pagination_size', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'news' => array(self::HAS_MANY, 'News', 'category_id'),
			'postQueues' => array(self::HAS_MANY, 'PostQueue', 'catgory_id'),
			'Subcategory' => array(self::HAS_MANY, 'SubCategories', 'category_id'),
			'Settings' => array(self::HAS_MANY, 'Settings', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$array = array(
			'id' => 'ID',
			'title' => 'Title',
			'url' => 'Url',
			'is_rss' => 'Is Rss',
			'deleted' => 'Deleted',
			'shorten_url' => 'shorten url',
			'active' => 'Status',
			'created_at' => 'Created At',
		);
		return array_merge($array,$this->Labels());
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);

		$criteria->compare('deleted',0);
		$criteria->compare('active',$this->active);

		$criteria->compare('created_at',$this->created_at,true);

		$criteria->order = 'title asc';
		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),
			'criteria'=>$criteria,
		));
	}


	public function get_category($order= true){

		$criteria=new CDbCriteria;
		if($order)
		$criteria->order = 'title asc';
		return $this->findAll($criteria);
	}


	public function generateTree()
	{
		$data = array();

		foreach ($this->findAll() as $category) {
			$data[$category->id] = array(
				'id'   => $category->id,
				'text' => '<a href="/admin/catalog/category/id/'.$category->id.'">'.$category->title.'</a>',

			);
			foreach ($category->Subcategory as $item) {
				$data[$category->id]['children'][$item->id] = array(
					'id'   => $item->id,
					'text' =>  '<a href="/admin/catalog/item/id/'.$item->id.'">'.$item->title.'</a>',
					'expanded' => false,
				);
			}

		}
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getNextId() {
		$record = self::model()->find(array(
			'condition' => 'id>:current_id',
			'order' => 'id ASC',
			'limit' => 1,
			'params' => array(':current_id' => $this->id),
		));
		if ($record !== null) {
			return $record;
		}
		return null;
	}
	public function getPreviousId() {
		$record = self::model()->find(array(
			'condition' => 'id<:current_id',
			'order' => 'id DESC',
			'limit' => 1,
			'params' => array(':current_id' => $this->id),
		));
		if ($record !== null) {
			return $record;
		}
		return null;
	}
}
