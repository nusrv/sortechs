<?php

class FillerDataController extends BaseController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getAll','index','view','edit','admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),
		/*	array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionTextTwitter(){


		if(isset($_POST['text']) and isset($_POST['type'])){

			$length = $this->getTweetLength($_POST['text'],$_POST['type']=='image'?true:false,$_POST['type']=='video');
			echo $length;

		}

	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $model = new FillerData('create');
        $model->setScenario('create');

        $this->performAjaxValidation($model);
        $valid = true;

        if (isset($_POST['FillerData'])) {
            $model->attributes = $_POST['FillerData'];
            $platforms = $_POST['FillerData'];
            if(empty($platforms['platform_id'] )){
                $model->addError('platform_id', 'you must select at least one platform');

            }

            if(!empty($platforms['platform_id'])) {
                foreach(array_reverse($platforms['platform_id']) as $item){
                    $model->platform_id = $item;

                    if($model->platform->title == 'Twitter'){
                        break;
                    }
                }

                $model->validate();
                $validTime = $this->validateBetweenDate($model, 'start_date', 'end_date');
                if ($this->validateTypeAndPlatform($model, $model->platform->title, 'type')) {
                    if ($model->type == 'image') {
                        $valid = $this->validateImageUploader($model, 'media_url', 'image_FillerData_' . time());
                    }
                    if ($model->type == 'video') {
                        if($this->validateVideo($model,'media_url',$model->platform->title)){

                            $valid = $this->validateVideoUploader($model, 'media_url', $model->platform->title, 'video_FillerData_' . time());
                        }else{
                            $valid = false;
                        }
                    }


                    if ($model->type == 'youtube') {
                        $valid = $this->Youtube($model, 'youtube', 'media_url', 'type', 'youtube_FillerData');
                        if ($valid) $valid = $this->validateDownloadVideoUploader($model, 'media_url', $model->platform->title, 'video_FillerData_' . time(), 'type', 'youtube');
                    }
                    if ($model->type == 'preview') {
                        if (!empty($model->link)) {
                            $shortUrl = new ShortUrl();
                            $model->link = $shortUrl->short(urldecode($model->link));
                        }

                    }
                }else
                    $valid = false;

                $model->created_at = date('Y-m-d H:i:s');
                 if($valid){
                    foreach ($platforms['platform_id'] as $item) {
                        $model->created_at = date('Y-m-d H:i:s');
                        //$model->last_generated = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'). ' -1 days'));
                        $model->last_generated = date('Y-m-d H:i:s');
                        $model->id = null;
                        $model->platform_id = $item;
                        $model->validate();


                        if ($valid && $validTime) {
                            $model->setIsNewRecord(true);
                            if ($model->save()) {
                            }
                        }
                    }
                }
            }

            if($model->id != null) {
				Yii::app()->user->setFlash('create', 'Thank you for add conetns ... . .... ');
				$this->redirect(array('view', 'id' => $model->id));
			}
            $model->platform_id = $platforms['platform_id'];

        }


        $this->render('create', array(
            'model' => $model,
        ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{

		$model = $this->loadModel($id);
		$model->scenario = 'update';
		$valid = true;
		$this->performAjaxValidation($model);
		if (isset($_POST['FillerData'])) {
			$Media = $model->media_url;
			$Type = $model->type;
			$Link = $model->link;
			$model->attributes = $_POST['FillerData'];

			$model->validate();
			$model->attributes = $_POST['FillerData'];

			if ($this->validateTypeAndPlatform($model, $model->platform->title, 'type')) {
				if ($model->type == 'image') {
					if (!$this->FileEmpty($model, 'media_url')) {
						if (!$this->GetFileType($model, $Media, 'image')) {
							$model->addError('media_url', 'media cannot be blank');
							$valid = false;
						} else {
							$model->media_url = $Media;
						}
					} else {
						if(!$this->GetFileType($model,'media_url','image','upload')){
							$model->addError('media_url',"It seems you Didn't upload a proper image file ..");
							$valid=false;
						}else
							$model->media_url = $this->uploader($model, 'media_url', 'filler_data_edit_id' . $model->id . '_' . time());
					}
				}
				if ($model->type == 'video') {
					if (!$this->FileEmpty($model, 'media_url')) {
						if (!$this->GetFileType($model, $Media, 'video')) {
							$model->addError('media_url', 'media cannot be blank');
							$valid = false;
						} else {
							$model->media_url = $Media;
						}
					} else {
						$valid = $this->validateVideoUploader($model, 'media_url', $model->platform->title, 'filler_data_edit_id' . $model->id . '_' . time());
					}

				}
				if ($model->type == 'youtube') {
					$valid = $this->Youtube($model, 'youtube', 'media_url', 'type', 'youtube_FillerData_');
					if ($valid) $valid = $this->validateDownloadVideoUploader($model, 'media_url', $model->platform->title, 'video_FillerData_' . time(), 'type', 'youtube');
				}

				if ($model->type == 'preview') {
					$model->media_url = $Media;
					if (!empty($model->link)) {
						if (strpos($model->link, 'goo.gl') == false) {
							if (empty($Link)) {
								if ($model->type == 'preview') {
									if (!empty($model->link)) {
										$shortUrl = new ShortUrl();
										$model->link = $shortUrl->short(urldecode($model->link));
									}
								}
							}
						}
					}
				}
			} else
				$valid = false;
			/*
                        if (!empty($Media)) {
                            if ($model->type == 'image' and pathinfo($Media)['extension'] == 'mp4') {
                                $model->media_url = $Media;
                                $valid = true;
                            } elseif ($model->type == 'video' and pathinfo($Media)['extension'] == 'mp4') {
                                $model->media_url = $Media;
                                $valid = true;
                            }
                        }*/

			$model->last_generated = date('Y-m-d H:i:s');
			if ($valid) {
				if ($model->save(true)) {
					Yii::app()->user->setFlash('update', 'Thank you for add conetns ... . .... ');
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('FillerData');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FillerData('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FillerData'])) {
			$this->data_search = $_GET['FillerData'];
			$this->data_search($model);
			$model->attributes = $_GET['FillerData'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FillerData the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FillerData::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FillerData $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='filler-data-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionGetAll(){
		if(isset($_POST['id'])){
			$id= $_POST['id'];
			$platform = Platform::model()->findByPk($id);
			echo CJSON::encode($array = Yii::app()->params['rule_array'][strtolower($platform->title)]);
		}
	}
    public function actionEdit(){
        if(isset($_POST)){

            if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
                $model = $this->loadModel($_POST['pk']);
                if($_POST['name'] == 'type'){
                    $_POST['value'] = ucfirst($_POST['value']);
                }
                if(!empty($_POST['name'])){
                    $name = $_POST['name'];
                    $value = $_POST['value'];
                    $model->$name = $value;
                    if($model->save())
                        echo true;
                    else
                        echo false;
                }
            }

        }
    }
}
