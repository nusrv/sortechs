<?php
/* @var $this DefaultController */
/* @var $total_posts DefaultController */
/* @var $model PostQueue */
Yii::app()->clientScript->registerScript('search', "
$('#search-form').change(function(){
if($('#PostQueue_schedule_date').val() != '' ){
    $(this).submit();
    }if($('#PostQueue_to').val() != '' ){
    $(this).submit();
    }
});
$('#PostQueue_schedule_date').attr('readonly','readonly');
$('#PostQueue_to').attr('readonly','readonly');
/*$('#yw28').contentChanged(function(){
});*/
");
?>
<?php

?>
<script>

    $(document).ready(function(){
        $('.thematics').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.thematic').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            }else if($(this).hasClass('fa-minus')){
                $('.thematic').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
            }
        });
        $('.nonthematics').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.Nonthematic').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            }else if($(this).hasClass('fa-minus')){
                $('.Nonthematic').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
            }
        });
        $('.all-automated').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.automated').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            }else if($(this).hasClass('fa-minus')){
                $('.automated').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
            }
        });
        $('.collapse-all').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.automated,.Nonthematic,.thematic').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
                $('.all-automated,.nonthematics,.thematics').removeClass('fa-plus');
                $('.all-automated,.nonthematics,.thematics').addClass('fa-minus');

            }else if($(this).hasClass('fa-minus')){
                $('.automated,.Nonthematic,.thematic').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
                $('.all-automated,.nonthematics,.thematics').addClass('fa-plus');
                $('.all-automated,.nonthematics,.thematics').removeClass('fa-minus');

            }
        });
    })
</script>
<style>
    .thematic,.Nonthematic,.automated{
        color:black;
        display: none;
        background-color: #d7edf4;
    }
    .fa{
        cursor: pointer;
    }
    td,th{
      /*  border-right:1px solid black;*/
        width:20%;
        overflow: auto;
        text-align: center;
    }
  
    h5{
        border-bottom:1px solid #229bc3;
    }
    td h6{
        border-bottom: 1px dotted #229bc3;
        max-width:90px;
        margin-left:15%;
        font-size:14px;
    }
    .collapse-all{
        margin-left:50px;
    }
    .column-color{
        background-color: #ADD8E6
    }
    td {
        height: 70px;
    }
</style>

    <div class="row">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Pushed posts report</h3>

                <?php $this->renderPartial('_adv_search_date', array('model' => $model,'this',$this)); ?>
                <br /><br />
            <div class="container-fluid">
                <div class="table-responsive">
                    <i class="fa fa-plus collapse-all" data-toggle="tooltip" title="Collapse all"></i>&nbsp;&nbsp;&nbsp;Collapse all
                    <table class="table">

                        <thead>
                        <tr class="heads">
                            <th><h5><b>Total/Platforms</b></h5></th>
                            <th class="column-color"><h5><strong>Facebook</strong></h5></th>
                            <th><h5><strong>Twitter</strong></h5></th>
                            <th class="column-color"><h5><strong>Instagram</strong></h5></th>
                            <th><h5><strong>Total</strong></h5></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="bg-lightgreen">
                            <td><h5><strong><i class="fa fa-plus thematics"></i>&nbsp;&nbsp;&nbsp;Thematic</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['thematic_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_thematic']) ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Preview</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_preview_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['thematic_preview_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_preview_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_thematic_preview']) ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Image</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_image_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['thematic_image_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_image_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_thematic_image']) ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Text</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_text_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['thematic_text_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_text_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_thematic_text']) ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Video</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_video_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['thematic_video_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['thematic_video_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_thematic_video']) ?></h6></td>

                        </tr>
                        <tr>
                            <td><h5><strong><i class="fa fa-plus nonthematics"></i>&nbsp;&nbsp;&nbsp;Non-Thematic</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['nonthematic_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_nonthematic']) ?></h6></td>
                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Preview</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_preview_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['nonthematic_preview_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_preview_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_nonthematic_preview']) ?></h6></td>
                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Image</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_image_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['nonthematic_image_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_image_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_nonthematic_image']) ?></h6></td>
                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Text</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_text_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['nonthematic_text_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_text_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_nonthematic_text']) ?></h6></td>

                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Video</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_video_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['nonthematic_video_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['nonthematic_video_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_nonthematic_video']) ?></h6></td>

                        </tr>
                        <tr>
                            <td><h5><strong><i class="fa fa-plus all-automated"></i>&nbsp;&nbsp;&nbsp;Automated posts</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['automated_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['automated_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['automated_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_automated']) ?></h6></td>
                        </tr>
                        <tr class="automated">
                            <td><h5>Edited</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['editedautomated_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['editedautomated_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['editedautomated_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_editedautomated']) ?></h6></td>

                        </tr>
                        <tr class="automated">
                            <td><h5>None edited</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['withouteditautomated_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['withouteditautomated_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['withouteditautomated_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_withouteditautomated']) ?></h6></td>
                        </tr>
                        <tr>
                            <td><h5><strong>Repushed posts</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['cloned_facebook']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['cloned_twitter']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts['cloned_instagram']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts['total_cloned']) ?></h6></td>
                        </tr>
                  <tr>
                      <td><h5><strong>All posts</strong></h5></td>
                      <td class="column-color"><h6><?php echo number_format($total_posts['all_posts_facebook']) ;?></h6></td>
                      <td><h6><?php echo number_format($total_posts['all_posts_twitter']) ;?></h6></td>
                      <td class="column-color"><h6><?php echo number_format($total_posts['all_posts_instagram']) ;?></h6></td>
                      <td><h6><?php echo number_format($total_posts['total_all_posts_instagram']) ;?></h6></td>
                  </tr>
                        </tbody>
                    </table>
                </div>
            </div>
</div>
            </div>
    </div>

