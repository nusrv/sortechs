<?php
/* @var $this GeneratorPostController */
/* @var $model GeneratorPost */
/* @var $form TbActiveForm */
?>
<style>
    .col-sm-12.main_image {
        margin-bottom: 20px;
    }
</style>
<script>
    $(document).ready(function () {

        $('input[type=checkbox]').change(function(){
            if($('#GeneratorPost_submit_to_all').is(':checked')){
                $('.Platforms').prop('checked',true);
            }
            var input=0;
            $type = $("#ytGeneratorPost_twitter_type");
            var post = $("#GeneratorPost_twitter_post").val();
            var num = 140;
            $.get('<?PHP echo CController::createUrl('getTwitterSize') ?>', {text: post,type:$("#ytGeneratorPost_twitter_type").val()}, function (data) {
                    input = num - data;
                    console.log(input);
                    $("#twitter_counter").text(input);
                    if (input > 0) {
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                    if($('#GeneratorPost_platforms_1').is(':checked') || $('#GeneratorPost_submit_to_all').is(':checked')) {
                        if(input<0) {
                            $("#schedule").hide();
                            $("#post_now_btn").hide();
                            $(".error-twitter").show();
                            $(".error-twitter").show();
                        }
                    }else{
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                }

            );
        });
        var input=0;
        $type = $("#ytGeneratorPost_twitter_type");
        var post = $("#GeneratorPost_twitter_post").val();
        var num = 140;
        $.get('<?PHP echo CController::createUrl('getTwitterSize') ?>', {text: post,type:$("#ytGeneratorPost_twitter_type").val()}, function (data) {
                input = num - data;
                console.log(input);
                $("#twitter_counter").text(input);
                if (input > 0) {
                    $("#schedule").show();
                    $("#post_now_btn").show();
                    $(".error-twitter").hide();
                    $(".error-twitter").hide();
                }
                if($('#GeneratorPost_platforms_1').is(':checked') || $('#GeneratorPost_submit_to_all').is(':checked')) {
                    if(input<0) {
                        $("#schedule").hide();
                        $("#post_now_btn").hide();
                        $(".error-twitter").show();
                        $(".error-twitter").show();
                    }
                }else{
                    $("#schedule").show();
                    $("#post_now_btn").show();
                    $(".error-twitter").hide();
                    $(".error-twitter").hide();
                }
            }

        );
        $('#GeneratorPost_twitter_post').keyup(function (e) {
            var input=0;
            $type = $("#ytGeneratorPost_twitter_type");
            var post = $("#GeneratorPost_twitter_post").val();
            var num = 140;
            $.get('<?PHP echo CController::createUrl('getTwitterSize') ?>', {text: post,type:$("#ytGeneratorPost_twitter_type").val()}, function (data) {
                    input = num - data;
                    console.log(input);
                    $("#twitter_counter").text(input);
                    if (input > 0) {
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                    if($('#GeneratorPost_platforms_1').is(':checked') || $('#GeneratorPost_submit_to_all').is(':checked')) {
                        if(input<0) {
                            $("#schedule").hide();
                            $("#post_now_btn").hide();
                            $(".error-twitter").show();
                            $(".error-twitter").show();
                        }
                    }else{
                        $("#schedule").show();
                        $("#post_now_btn").show();
                        $(".error-twitter").hide();
                        $(".error-twitter").hide();
                    }
                }

            );
        });
        var image_pr = $('#ytGeneratorPost_image');
        var main_image = $('.main_image');

        if(image_pr.val() != ''){

            main_image.append('<img src="'+image_pr.val()+'" class="img-responsive main_image_changer">');
        }
        $('.image_changer').click(function () {
            $('.image_changer').addClass('grayscale');
            $('.main_image_changer').attr('src',$(this).attr('src'));
            $('#ytGeneratorPost_image').val($(this).attr('src'));
            $(this).removeClass('grayscale');

        });

        $("#post_now_btn").click(function(e) {
            $(this).hide();
            $('#schedule_btn').hide();
        });
        $("#schedule_btn").click(function(e) {
            $(this).hide();
            $('#post_now_btn').hide();
        });

        $('#generator-post-form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $('#GeneratorPost_link').change(function(){
            //loading
            $('#loading2').css('display','block');
            $('#generator-post-form').submit();
        });
        $('#GeneratorPost_link').on('paste', function () {
            setTimeout(function () {
                $('#loading2').css('display','block');
                $('#generator-post-form').submit();
            }, 100);
        });


        $("#GeneratorPost_submit_to_all").click(function () {
            if ($(this).is(':checked')) {
                $('.Platforms').prop('disabled', true);
            } else {
                $('.Platforms').prop('disabled', false);
            }
        });
        $('#GeneratorPost_time').datetimepicker({
            format: 'Y-m-d H:i', step: 5,
            minDate: new Date('Y-m-d H:i')
        });
        /*$url = $('#GeneratorPost_link');
         $link = $('#GeneratorPost_link');*/
    });

</script>


<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <div class="col-sm-9"></div>
            <div class="col-sm-3" style="text-align: left;">
                <?php echo Yii::app()->params['statement']['previousPage']; ?>
            </div>
        </div>
        <div class="box-body">

            <?php echo TbHtml::link('Non thematic',array('/postQueue/create'),array('id'=>'btn_12','class'=>'btn btn-active'))?>
            <?php echo TbHtml::button('Thematic',array('/thematic_post'),array('class'=>'btn btn-active'))?>
            <hr style="margin-top: 20px">


            <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
                'id' => 'generator-post-form',
                'enableAjaxValidation' => true,
                'type' => 'horizontal',
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            ));
            echo CHtml::hiddenField('GeneratorPost[image]', $model->image, array('name' => '', 'id' => 'ytGeneratorPost_image'));
            echo CHtml::hiddenField('GeneratorPost[preview]', $model->preview, array('name' => '', 'id' => 'ytGeneratorPost_preview'));
            echo CHtml::hiddenField('GeneratorPost[submits]', $model->submits, array('name' => '', 'id' => 'ytGeneratorPost_submits'));
            echo CHtml::hiddenField('GeneratorPost[title]', $model->title, array('name' => '', 'id' => 'ytGeneratorPost_title'));
            echo CHtml::hiddenField('GeneratorPost[description]', $model->description, array('name' => '', 'id' => 'ytGeneratorPost_description'));
            echo CHtml::hiddenField('GeneratorPost[category_id]', $model->category_id, array('name' => '', 'id' => 'ytGeneratorPost_category_id'));
            echo CHtml::hiddenField('GeneratorPost[sub_category_id]', $model->sub_category_id, array('name' => '', 'id' => 'ytGeneratorPost_sub_category_id'));
            echo CHtml::hiddenField('GeneratorPost[column]', $model->column, array('name' => '', 'id' => 'ytGeneratorPost_column'));
            echo CHtml::hiddenField('GeneratorPost[creator]', $model->creator, array('name' => '', 'id' => 'ytGeneratorPost_creator'));
            echo CHtml::hiddenField('GeneratorPost[link_md5]', $model->link_md5, array('name' => '', 'id' => 'ytGeneratorPost_link_md5'));
            echo CHtml::hiddenField('GeneratorPost[shorten_url]', $model->shorten_url, array('name' => '', 'id' => 'ytGeneratorPost_shorten_url'));
            echo CHtml::hiddenField('GeneratorPost[facebook_type]', $model->facebook_type, array('name' => '', 'id' => 'ytGeneratorPost_facebook_type'));
            echo CHtml::hiddenField('GeneratorPost[instagram_type]', $model->instagram_type, array('name' => '', 'id' => 'ytGeneratorPost_instagram_type'));
            echo CHtml::hiddenField('GeneratorPost[twitter_type]', $model->twitter_type, array('name' => '', 'id' => 'ytGeneratorPost_twitter_type'));
            echo CHtml::hiddenField('GeneratorPost[new_category]', $model->new_category, array('name' => '', 'id' => 'new_category'));

            ?>

            <div class="col-sm-5 ">
                <?php
                echo $form->urlFieldGroup($model, 'link',
                    array(
                        'labelOptions' => array('class' => 'col-sm-2'),
                        'wrapperHtmlOptions' => array('class' => 'col-sm-10',)
                    ));
                ?>
                <hr>
                <?php echo $form->textFieldGroup($model, 'time',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',
                        ),
                        //  'append' => $form->checkboxGroup($model,'now',array('groupOptions'=>array('style'=>'    margin: -4px;'))),
                        'groupOptions' => array(),
                    )
                );
                ?>
                <hr>
                <?php
                echo $form->checkboxGroup($model, 'pinned', array(
                    'label' => null,
                    'Class_span' => 'col-sm-0',
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-4 col-sm-push-2',
                    )
                ));
                ?>
                <hr>

                <div id="platforms">
                    <?php
                    $model->platforms = array('Facebook' => true, 'Twitter' => true, 'Instagram' => true);
                    echo $form->checkboxGroup($model, 'submit_to_all', array(
                        'widgetOptions' => array(
                            'htmlOptions' => array(
                                'checked' => 'checked'
                            ),
                        ),
                        'label' => null,
                        'Class_span' => 'col-sm-0',
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-4 col-sm-push-2',

                        ),
                    ));
                    echo $form->checkboxListGroup($model, 'platforms', array(
                        'widgetOptions' => array(
                            'data' => CHtml::listData(Platform::model()->findAll('deleted=0'), 'id', 'title'),
                            'htmlOptions' => array(
                                'class' => 'Platforms',
                                'disabled' => true
                            ),
                        ),
                        'labelOptions' => array(
                            'class' => 'col-sm-2 platforms'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-8 platforms',
                        ),
                    ));
                    ?>
                </div>
                <div class="col-sm-12 massage" style="display: none">
                    <div class="alert alert-warning">
                        please wait a moment
                    </div>
                </div>
                <div class="form-actions  pull-right">
                    <div class="error-twitter" style="display: none;">
                        <div class="alert alert-warning">
                            <h4>Please check twitter characters count</h4>
                        </div>
                    </div>

                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'context' => 'success',
                            'label' => 'Post now',
                            'htmlOptions' => array('style' => '    margin-right: 12px;','id'=>'post_now_btn','name'=>'post_now_btn')
                        )
                    );
                    ?>
                    <?php

                    if($model->schedule){
                        $this->widget(
                            'booster.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'context' => 'primary',
                                'label' => 'schedule',

                                'htmlOptions' => array('style' => '', 'id'=>'schedule','name'=>'schedule_btn')
                            )
                        );
                    }
                    else{
                        $this->widget(
                            'booster.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'context' => 'primary',
                                'label' => 'schedule',

                                'htmlOptions' => array('style' => '', 'id'=>'schedule','name'=>'schedule_btn',   'disabled'=>'disabled',)
                            )
                        );
                    }
                    ?>

                </div>

            </div>
            <div class="col-sm-7 col-xs-12">
                <div class="row">
                    <div id="link-img">
                        <div id="loading2" style="display: none;margin:0 auto;" align="center">
                            <h3 class="alert alert" style="background-color: #ddd">
                                <b>Please wait a moment ...</b><br>
                                The preview might take  some time to process ,<br>
                                You may still push the update anyways.

                            </h3>
                            <p><img src="<?php echo Yii::app()->baseUrl . "/image/updateimg.gif" ?>" class="loading-imge"/></p>
                        </div>
                    </div>
                    <div class="col-sm-12 main_image"></div>
                </div>
                <?php echo $form->textAreaGroup($model, 'facebook_post',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',
                        ),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                'style'=>'height: 110px;',
                                'placeholder'=>'Facebook',
                                //'readonly'=>'readonly',
                            )
                        )
                    )
                );
                ?>
                <hr>
                <?php echo $form->textAreaGroup($model, 'instagram_post',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',
                        ),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array('style'=>'height: 110px;',
                                'placeholder'=>'Instagram',

                                // 'readonly'=>'readonly',
                            )
                        )
                    )
                );
                ?>
                <hr>
                <div class="col-xs-12 col-sm-push-2" id="twitter" style="/*display:none;*/">
                    Number of Characters:
                    <small id="twitter_counter" style="color:red;font-size: 18px">140</small>

                </div>
                <?php echo $form->textAreaGroup($model, 'twitter_post',
                    array(
                        'labelOptions' => array(
                            'class' => 'col-sm-2'
                        ),
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-10',

                        ),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                'style'=>'height: 110px;',
                                'placeholder'=>'Twitter',

                                // 'readonly'=>'readonly',

                            )
                        )
                    )
                );
                ?>
            </div>
            <?php $this->endWidget(); ?>

            <div class="row">
                <div class="col-xs-12 title" style="direction: rtl">
                    <h3><b>Title : </b><small style="color:black;">
                            <?php echo $model->title?>
                        </small></h3>
                </div>
                <div class="col-xs-12 description" style="direction: rtl">
                    <h4><b>Description :</b> <small style="color:black;">
                            <?php echo $model->description?>
                        </small></h4>
                </div>
            </div>
            <div class="col-xs-12 images">
                <?php
                if(!empty($model->gallary))
                    foreach ($model->gallary as $item): ?>
                        <div class="col-sm-3">
                            <img src="<?php echo $item?>" class="img-responsive img-thumbnail grayscale image_changer"/>
                        </div>
                    <?php endforeach; ?>
            </div>
        </div>

    </div>
    </div>
</section>




