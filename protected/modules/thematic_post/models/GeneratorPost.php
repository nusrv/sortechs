<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 * * @property integer $id
 * @property integer $category_id
 * @property integer $sub_category_id
 * @property string $title
 * @property string $column
 * @property string $link
 * @property string $link_md5
 * @property string $description
 * @property string $shorten_url
 * @property string $publishing_date
 * @property string $schedule_date
 * @property integer $generated
 * @property string $created_at
 * @property string $creator
 */
class GeneratorPost extends ThematicPostBaseCFormModel
{
    public $link;
    public $time;
    public $submit_to_all = true;
    public $platforms;
    public $anyway;
    public $pinned = 0;
    public $now = false;
    public $image = null;
    public $preview = 0;
    public $submits = 0;
    public $facebook_post = null;
    public $instagram_post = null;
    public $twitter_post = null;
    public $facebook_type = null;
    public $instagram_type = null;
    public $twitter_type = null;
    public $title = null;
    public $description = null;
    public $category_id = null;
    public $sub_category_id = null;
    public $link_md5 = null;
    public $shorten_url = null;
    public $creator = null;
    public $column = null;
    public $new_category =null;
    public $schedule= 0;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('link', 'required'),
            array('link', 'check_emarate'),
            array('submit_to_all,platforms', 'checkBoxes_checked'),

        );
    }
    public function check_emarate($attr){

        $link = Yii::app()->params['feedUrl'];
        $pos = strpos(' '.$this->link, $link );
        if(!$pos) {
            $this->addError('link', 'only links with (http://www.thenational.ae) is accepted');
            return false;
        }
        return true;
    }

    public function checkBoxes_checked($attr){
        if(empty($this->submit_to_all) and empty($this->platforms)){
            $this->addError('platforms', 'Please select at least one platform or check submit to all');
            return false;

        }else{
            return true;
        }
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'submit_to_all' => 'submit to all',
            'link' => 'Url ',
            'time'   => 'Scheduled date',
            'now'   => 'Post now ',
            'twitter_post'   => '<span class="fa fa-twitter"></span> Twitter',
            'facebook_post'   => '<span class="fa fa-facebook"></span> Facebook',
            'instagram_post'   => '<span class="fa fa-instagram"></span> Instagram',
        );
    }


}
