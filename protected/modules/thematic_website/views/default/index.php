<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'thematic-website-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
$field = 'col-sm-10';
$label = 'col-sm-2';
?>
<section class="content">
	<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<?php
			$categories = Category::model()->findAll();
			foreach($categories as $category){
				?>
				<div class="col-xs-2">
					<input type="button" class="btn btn-success" value="<?php echo $category->title ?>" id="category_<?php echo $category->id ?>" />
				</div>
			<?php
			}
			?>
		</div>
		</div>
	</div>
</section>
<?php $this->endWidget(); ?>
