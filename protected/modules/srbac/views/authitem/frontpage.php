<?php
/**
 * frontpage.php
 *
 * @author Spyros Soldatos <spyros@valor.gr>
 * @link http://code.google.com/p/srbac/
 */

/**
 * Srbac main administration page
 *
 * @author Spyros Soldatos <spyros@valor.gr>
 * @package srbac.views.authitem
 * @since 1.0.2
 */
?>
<style>
    .content {
        min-height: 114px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
        padding-bottom: 0px;
        padding-top: 0px;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                </div>

                <div class="box-body">
                    <div class="col-sm-4 text-center">
                        <i class="ion ion-network"></i>
                        <?php
                    echo CHtml::link(Helper::translate('srbac','Managing auth items') ,array('authitem/manage'));
                    ?>
                    </div>
                    <div class="col-sm-4 text-center">
                        <i class="ion ion-key"></i>
                        <?php
                    echo CHtml::link(Helper::translate('srbac','Assign to users') ,array('authitem/assign'));
                    ?>
                    </div>
                    <div class="col-sm-4 text-center">
                        <i class="ion ion-person-stalker"></i>
                        <?php
                        echo CHtml::link(Helper::translate('srbac','User\'s assignments') ,array('authitem/assignments'));
                        ?>
                    </div>
                </div>
                </div>
            </div>
        </div>
</section>