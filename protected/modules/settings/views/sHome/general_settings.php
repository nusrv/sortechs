<?php
/* @var $this SHomeController */
/* @var $model Settings */
/* @var $form TbActiveForm */
?>

        <script>
            $(document).ready(function() {
                $('.StartTime').datetimepicker({
                    datepicker:false,
                    format:'H:i:00',
                    step:30
                });
                $('.EndTime').datetimepicker({
                    datepicker:false,
                    format:'H:i:00',
                    step:30
                });

            $('#settings-form').submit(function (e) {
                var send = '<div class="alert alert-info"><strong>Info!</strong> Loading....... </div>';
                var complate = '<div class="alert alert-success"><strong>success!</strong> Indicates a successful or positive action </div>';
                $(this).loadingBox('.loading-page',send,complate);

                $.ajax({
                    type: "POST",
                    url: $(this).attr('action'),
                    data:$(this).serialize(),
                    timeout: 10000, // in milliseconds
                    success: function(data) {
                        $('.msg').append('<div class="alert alert-success">' +
                            '<strong>Success!</strong>' +
                            'Indicates a successful or positive action' +
                            '</div>').fadeOut(4000);
                    },
                    error: function(request, status, err) {
                        if(status == "timeout") {
                            $('.msg').append('<div class="alert alert-warning">' +
                                '<strong>Danger!</strong>' +
                                'This action took too much time to process\n Please try again' +
                                '</div>').fadeOut(2000);
                        }
                    }
                });
                return false;
            });
            });

        </script>
        <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
            'id'=>'settings-form',
            'action'=>$this->createUrl('sHome/index'),
            'enableAjaxValidation'=>true,
            'type'=>'horizontal',
        )); ?>


            <!--<div class="col-sm-6">
                <div class="col-sm-12">
                    <h2 class="page-header">Time</h2>
                </div>

                <div class="col-sm-12 Scheduled-body">
                    <h4 class="Scheduled-title"><b>Scheduled</b></h4>
                    <?php /*echo $form->timeFieldGroup($model,'start_date_time',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'StartTime')))); */?>

                    <?php /*echo $form->timeFieldGroup($model,'end_date_time',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'EndTime')))); */?>
                </div>

                <div class="col-sm-12 Scheduled-body">
                    <h4 class="Scheduled-title"><b>Direct Push</b></h4>
                    <?php
/*                    echo $form->checkboxGroup($model, 'direct_push',
                        array(
                            'widgetOptions' => array(

                            )
                        )
                    );
                    */?>
                    <?php /*echo $form->timeFieldGroup($model,'direct_push_start',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'StartTime')))); */?>

                    <?php /*echo $form->timeFieldGroup($model,'direct_push_end',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'EndTime')))); */?>
                </div>
            </div>-->

            <div class="col-sm-12">
                <div class="col-sm-12">
                    <h2 class="page-header">Configuration</h2>
                </div>

                <div class="col-sm-12 Scheduled-body">
                    <h4 class="Scheduled-title"><b>Options</b></h4>
                   <!-- <?php /*echo $form->dropDownListGroup($model,'how_many_posts',array('widgetOptions'=>array('data'=>$model->all_numbers()))); */?>
                    --><?php /*echo $form->dropDownListGroup($model,'gap_time',array('widgetOptions'=>array('data'=>$model->gap_time()))); */?>
                    <?php echo $form->dropDownListGroup($model,'timezone',array('widgetOptions'=>array('data'=>$model->getTimeZone()))); ?>
                    <?php /*echo $form->dropDownListGroup($model,'pinned',array('widgetOptions'=>array('data'=>array('0'=>'No','1'=>'yes'))));*/?>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-actions pull-right">
                    <?php $this->widget(
                        'booster.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'context' => 'primary',
                            'label' =>'Save',
                        )
                    );
                    ?>
                </div>
            </div>
        <?php $this->endWidget(); ?>
