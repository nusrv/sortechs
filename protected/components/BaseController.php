<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class BaseController extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $data_search = null;

    public $creator;
    public $sub_category;
    public $source;
    public $news;
    public $PostQueue;
    public $trim_hash;
    public $hash_tag;

    public $details;
    public $category;
    public $model;
    public $platform;
    public $parent = null;

    public function beforeAction($action) {

        if($action->getId() == 'mainUn'){
            Yii::app()->session['mains']='postQueue/mainUn';
        }elseif($action->getId() == 'main'){
            Yii::app()->session['mains']='postQueue/main';
        }elseif($action->getId() == 'mainPinned'){
            Yii::app()->session['mains']='postQueue/mainPinned';
        }elseif($action->getId() == 'mainPosted'){
            Yii::app()->session['mains']='postQueue/mainPosted';
        }

        if($action->getId() != 'login' && !isset($_POST['LoginForm'])){
            if (Yii::app()->user->isGuest){
                $this->redirect(array('/site/login'));
                return false;
            }else{
                return true;
            }
        }else{

            Yii::import("application.modules.settings.models.Settings", true);

            $settings = Settings::model()->findByPk(1);
            if(!empty($settings))
                Yii::app()->timeZone  = $settings->timezone;
            else
                Yii::app()->timeZone = 'Asia/Dubai';
            if(!empty($data))
                Yii::app()->timeZone = empty($data->timezone)?'Asia/Dubai':$data->timezone;
            return true;
        }
    }

    public function bitShort($url){
        $d = Yii::app()->bitly->shorten($url)->getResponseData();
        $d = CJSON::decode($d);
        if(isset($d['status_code']) && $d['status_code'] == 200){
            if(isset($d['data'])){
                $data = $d['data'];
                if(isset($data['url'])){
                    return $data['url'];
                }
            }
        }

        return null;
    }

    public function data_search($model){

        $model->visible_id = isset($this->data_search['visible_id'])?$this->data_search['visible_id']:$model->visible_id;
        $model->visible_url = isset($this->data_search['visible_url'])?$this->data_search['visible_url']:$model->visible_url;
        $model->visible_title = isset($this->data_search['visible_title'])?$this->data_search['visible_title']:$model->visible_title;
        $model->visible_created_at = isset($this->data_search['visible_created_at'])?$this->data_search['visible_created_at']:$model->visible_created_at ;
        $model->visible_active = isset($this->data_search['visible_active'])?$this->data_search['visible_active']:$model->visible_active ;
        $model->visible_created_by = isset($this->data_search['visible_created_by'])?$this->data_search['visible_created_by']:$model->visible_created_by ;
        $model->visible_updated_by = isset($this->data_search['visible_updated_by'])?$this->data_search['visible_updated_by']:$model->visible_updated_by ;
        $model->visible_created_at = isset($this->data_search['visible_created_at'])?$this->data_search['visible_created_at']:$model->visible_created_at ;
        $model->visible_updated_at = isset($this->data_search['visible_updated_at'])?$this->data_search['visible_updated_at']:$model->visible_updated_at ;
        $model->visible_category_id = isset($this->data_search['visible_category_id'])?$this->data_search['visible_category_id']:$model->visible_category_id ;
        $model->visible_f_name = isset($this->data_search['visible_f_name'])?$this->data_search['visible_f_name']:$model->visible_f_name ;
        $model->visible_l_name = isset($this->data_search['visible_l_name'])?$this->data_search['visible_l_name']:$model->visible_l_name ;
        $model->visible_email = isset($this->data_search['visible_email'])?$this->data_search['visible_email']:$model->visible_email ;
        $model->visible_name = isset($this->data_search['visible_name'])?$this->data_search['visible_name']:$model->visible_name ;
        $model->visible_username = isset($this->data_search['visible_username'])?$this->data_search['visible_username']:$model->visible_username ;
        $model->visible_gender = isset($this->data_search['visible_gender'])?$this->data_search['visible_gender']:$model->visible_gender ;
        $model->visible_profile_pic = isset($this->data_search['visible_profile_pic'])?$this->data_search['visible_profile_pic']:$model->visible_profile_pic ;
        $model->visible_background_color = isset($this->data_search['visible_background_color'])?$this->data_search['visible_background_color']:$model->visible_background_color ;
        $model->visible_from_date = isset($this->data_search['visible_from_date'])?$this->data_search['visible_from_date']:$model->visible_from_date ;
        $model->visible_to_date = isset($this->data_search['visible_to_date'])?$this->data_search['visible_to_date']:$model->visible_to_date ;
        $model->visible_media_url = isset($this->data_search['visible_media_url'])?$this->data_search['visible_media_url']:$model->visible_media_url ;
        $model->visible_schedule_date = isset($this->data_search['visible_schedule_date'])?$this->data_search['visible_schedule_date']:$model->visible_schedule_date ;
        $model->visible_platform_id = isset($this->data_search['visible_platform_id'])?$this->data_search['visible_platform_id']:$model->visible_platform_id ;
        $model->visible_type = isset($this->data_search['visible_type'])?$this->data_search['visible_type']:$model->visible_type ;
        $model->visible_text = isset($this->data_search['visible_text'])?$this->data_search['visible_text']:$model->visible_text ;
        $model->visible_start_date = isset($this->data_search['visible_start_date'])?$this->data_search['visible_start_date']:$model->visible_start_date ;
        $model->visible_end_date = isset($this->data_search['visible_end_date'])?$this->data_search['visible_end_date']:$model->visible_end_date ;
        $model->visible_last_generated= isset($this->data_search['visible_last_generated'])?$this->data_search['visible_last_generated']:$model->visible_last_generated ;
        $model->visible_media_url= isset($this->data_search['visible_media_url'])?$this->data_search['visible_media_url']:$model->visible_media_url ;
        $model->visible_publish_time= isset($this->data_search['visible_publish_time'])?$this->data_search['visible_publish_time']:$model->visible_publish_time ;
        $model->visible_catgory_id= isset($this->data_search['visible_catgory_id'])?$this->data_search['visible_catgory_id']:$model->visible_catgory_id ;
        $model->visible_is_posted= isset($this->data_search['visible_is_posted'])?$this->data_search['visible_is_posted']:$model->visible_is_posted ;

    }

    public function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    public function uploader($model, $field_name, $name,$type =null)
    {
        $uploadedFile = CUploadedFile::getInstance($model, $field_name);
        $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);
        $path = $uploadedFile->name;
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
        $filename = $name . '.' . $ext;
        $fileUrl = false;
        if($type==null)
            $this->image_quality($uploadedFile->tempName);
        if ($s3->putObjectFile($uploadedFile->tempName, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'] . '/' . $filename, S3::ACL_PUBLIC_READ)) {
            $fileUrl = 'http://s3.amazonaws.com/' . Yii::app()->params['amazonBucket'] . '/' . Yii::app()->params['amazonFolder'] . '/' . $filename;
        }

        return $fileUrl;
    }

    public function image_quality($image){
        $obj = new \Imagick();
        $obj->readImage($image);
        $obj->thumbnailImage(500,500);
        $obj->setImageFormat('jpg');
        $obj->writeImage($image);
    }

    public function uploaderFile($file,$name){
        Yii::import('application.components.S3');
        if(!empty($file)){
            $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);
            $path = $file;
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
            $filename=$name.time().'.'.$ext;
            $fileUrl='';
            if ($s3->putObjectFile($file, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'].'/'.$filename , S3::ACL_PUBLIC_READ)) {
                $fileUrl = 'http://s3.amazonaws.com/'.Yii::app()->params['amazonBucket'].'/'.Yii::app()->params['amazonFolder'].'/'.$filename;
            }
            unlink($file);
            return array(true,$fileUrl);
        }else{
            return array(false,'');
        }
    }

    public function DownloaderYoutube($url, $form)
    {
        $rx = '~^(?:https?://)?(?:www\.)?(?:youtube\.com|youtu\.be)/watch\?v=([^&]+)~x';


        if(preg_match($rx,$url, $matches)){

            $download_video_url = "'$url'";
            exec("youtube-dl -F $download_video_url", $out);
            if (!empty($out)) {



                if (isset(explode(' ', $out[count($out) - 1])[0])) {
                    $id = explode(' ', $out[count($out) - 1])[0];
                    $path = Yii::app()->params['webroot'] . '/image/youtube_' . $form . '_' . time() . '.mp4';

                    exec("youtube-dl -o '$path' $download_video_url -f '$id'", $output);

                    if (strpos(end($output), '100%') !== false) {
                        return array(true,$path);
                    } else {
                        if (is_dir($path))
                            // unlink($path);
                            return array( false,'fail');
                    }
                } else
                    return array(false,'url');
            } else
                return array(false, 'url');

        }
        return array(false,'url');

    }

    public function FileEmpty($model, $field_name)
    {
        $uploadedFile = CUploadedFile::getInstance($model, $field_name);
        if (!empty($uploadedFile)) {
            return true;
        } else {
            return false;
        }
    }

    public function FileIsExist($Url)
    {
        return file_exists($Url);
    }

    public function FileType($model, $field_name, $type, $file = null)
    {

        if ($file == null) {
            $file = explode('.', $file);
            $ext = strtolower(end($file));
            if ($ext == 'png' or $ext == 'jpg') {
                return 'image';
            } elseif ($ext == 'mp4') {
                return 'video';
            }
            return false;

        } else {
            $uploadedFile = CUploadedFile::getInstance($model, $field_name);
            if (!empty($uploadedFile)) {
                $upload_type = strtolower($uploadedFile->extensionName);
                if ($type == 'image') {
                    if (!($upload_type == 'png' or $upload_type == 'jpg')) {
                        return false;
                    }
                    return true;
                } elseif ($type == 'video') {
                    if (!($upload_type == 'mp4')) {
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        }
    }

    public function GetFileType($model, $field_name, $type, $file = null)
    {

        $valid = true;

        if($file == null){
            $ex= explode('.', $field_name);
            $ext = strtolower(end($ex));
        }
        else{
            $uploadedFile = CUploadedFile::getInstance($model, $field_name);
            $ext = strtolower($uploadedFile->extensionName);
        }
        if ($type == 'image') {
            if ($ext == 'png' or $ext == 'jpg') {
                return $valid;
            }
        } elseif ($type == 'video') {
            if ($ext == 'mp4') {
                return $valid;

            }

        }
        return $valid = false;
    }


    public function Youtube($model,$Youtube,$media,$type,$name_file){

        $valid =false;

        if(!empty($model->$Youtube)){
            list($res,$path) = $this->DownloaderYoutube($model->$Youtube,$name_file);
            if($res)
            {
                $model->$type = 'video';
                $model->$media = $path;
                $valid =true;
                /* list($res,$path) =$this->uploaderFile($path,$name_file);
                 if($res) {
                     $model->$type = 'video';
                     $model->$media = $path;
                     $valid =true;
                 }else{
                     $valid = false;
                     $model->addError($Youtube,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                 }*/
            }
            else
                if($path == 'fail'){
                    $valid = false;
                    $model->addError($Youtube,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                }elseif($path == 'url'){
                    $valid = false;
                    $model->addError($Youtube,"<b>oops!!! :( </b><br>This Seems to be an Invalid URL , Please make sure your URL is correct.<br>ex. : https://www.youtube.com/watch?v=<b>XXXXXXX</b>");
                }
        }else{
            $valid = false;
            $model->addError($Youtube,"youtube link required.");
        }

        return $valid;

    }
    public function news($url){

        $link = Yii::app()->params['feedUrl'];

        if(!isset($url) && empty($url))
            exit();

        $check_link = strpos(' '.$url,$link);


        if($check_link){
            $category = explode($link,$url);
            if(is_array($category) && isset($category[1]) && !empty($category[1])){
                $split = explode('/',$category[1]);
                if(is_array($split) && isset($split[1]) && !empty($split[1]))
                    $category = Category::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'].'/'.$split[1])));
                else
                    $category = null;
                if(is_array($split) && isset($split[2]) && !empty($split[2]))
                    $sub_category = SubCategories::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'].'/'.$split[2])));
                else
                    $sub_category  = null;
            }else{
                $sub_category  = null;
                $category= null;
            }


            $info = array();

            $info['sub_category_id'] = null;
            $info['creator'] = null;
            if(isset($category->id)){
                $info['category_id'] = $category->id;
            }
            if(isset($sub_category->id)){
                $info['sub_category_id'] = $sub_category->id;
            }





            $info = array_merge($info,$this->get_info($url));

            $shorten_url =$this->bitShort(urldecode($info['link']));
            $info['shorten_url'] = $shorten_url;
            $info['link_md5'] = md5(urldecode($info['link']));

            return $this->AddNews($info);


        }

        return false;

    }
    public function get_info($url)
    {
        $html = new SimpleHTMLDOM();

        $html_data = $html->file_get_html($url);

        $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        $data['image']['type']='image';
        $data['title']  = null;
        $data['description']  = null;
        $data['link']  = $url;
        $data['link_md5']  = md5($url);
        $data['creator'] = null;
        $data['column'] = null;

        if(isset($html_data->find('meta[property=og:title]', 0)->content))
            $data['title']= $this->clear_tags($html_data->find('meta[property=og:title]', 0)->content);

        if(isset($html_data->find('meta[name=twitter:description]', 0)->content) and !empty($html_data->find('meta[name=twitter:description]', 0)->content))
            $data['description']= $this->clear_tags($html_data->find('meta[name=twitter:description]', 0)->content);
        /*elseif(isset($html_data->find('meta[property=og:title]', 0)->content))
            $data['description']= $this->clear_tags($html_data->find('meta[property=og:title]', 0)->content);*/
        else{
            $body_description = $html_data->find('div[itemprop=articleBody] p');
            foreach($body_description as $description){
                if(!$description->find('a'))
                    $data['description'] .= $description->plaintext;
            }
        }
        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'twitter:image:src') !== false) {
                $data['image']['src'] = $item->getAttribute('content');
                $data['image']['type']='image';
            }
        }
        $tags = 'div[class=textpic-box halfwidthheightratio]';
        $counter=0;
        if(!empty($html_data->find($tags))){
            $tags = 'div[class=textpic-box halfwidthheightratio] img';
            $find = $html_data->find($tags);
            foreach ($find as $item) {
                $data['gallery']['src'] =  $item->src;
                $data['gallary']['type']='gallery';
                $counter++;
            }
        }

        if(isset($html_data->find('a[rel=author]', 0)->innertext))
            $data['creator'] = $this->clear_author($html_data->find('a[rel=author]', 0)->innertext);


        return $data;
    }


    public function validateTypeAndPlatform($model,$platform,$type){

        $Type = $model->$type;

        $valid =true;

        if($platform  == 'Instagram' AND !in_array($Type,Yii::app()->params['rule_array']['instagram'])){
            $valid = false;
            $model->addError($type,"the instagram not support video use image .");
        }

        if($platform == 'Twitter' AND !in_array($Type,Yii::app()->params['rule_array']['twitter'])){
            $valid = false;
            $model->addError($type,"the Twitter not support $Type use <small>image,video,text,preview</small>.");
        }

        return $valid;

    }

    public function validateBetweenDate($model,$start,$end)
    {
        if (!((strtotime($model->$start)) > (strtotime($model->$end))))
        {
            return true;
        }
        $model->addError($start,"$start must be smaller than $end .");
        $model->addError($end,"$end must be greater than $start .");
        return false;
    }

    public function SizeVideo($model,$media,$size,$time,$type = false){

        Yii::import('application.extensions.getid3.getID3');
        $getID3 = new getID3;
        $valid =true;
        if($type){
            $uploadedFile=CUploadedFile::getInstance($model,$media);
            $file = $getID3->analyze($uploadedFile->tempName);

        }else{
            $video = Yii::app()->params['webroot'].'/video/size_video.mp4';
            file_put_contents($video,file_get_contents($model->$media));
            $file = $getID3->analyze($video);
        }
        if(isset($file['filesize'])){

            if($file['filesize']<$size){

                if(isset($file['playtime_string'])){

                    $times = explode(':',$file['playtime_string']);

                    if(isset($times[0]) && isset($times[1])){
                        $times[0] = $times[0] * 60;
                        $time_array = $times[0] + $times[1];
                        if($time_array<=$time){

                            return $valid;
                        }else{
                            $valid = false;
                            $model->addError($media,"Video size length is not compatible with Twitter guidelines <small>140 seconds</small>...");
                        }
                    }else{
                        $valid = false;
                        $model->addError($media,"Video size length is not compatible with Twitter guidelines <small>140 seconds</small>...");
                    }
                }else{
                    $valid = false;
                    $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                }
            }else{
                $valid = false;
                $model->addError($media,"Video size length is not compatible with Twitter guidelines <small>100MB</small>...");
            }
        }else{
            $valid = false;
            $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
        }

        return $valid;

    }

    public function validateImageUploader($model,$media,$name)
    {
        $valid = true;

        if($this->FileEmpty($model,$media)){
            if(!$this->GetFileType($model,$media,'image','upload')){
                $model->addError($media,"It seems you Didn't upload a proper image file ..");
                $valid=false;
            }else{
                $model->$media = $this->uploader($model,$media,$name);
                if($model->$media == false){
                    $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                    $valid=false;
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function validateProfileImageUploader($model,$media,$name)
    {
        $valid = true;

        if($this->FileEmpty($model,$media)){
            if(!$this->GetFileType($model,$media,'image','upload')){
                $model->addError($media,"It seems you Didn't upload a proper image file ..");
                $valid=false;
            }else{
                $model->$media = $this->uploader($model,$media,$name);
                if($model->$media == false){
                    $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                    $valid=false;
                }
            }
        }
        return $valid;
    }

    public function validateImageUpdate($model,$media)
    {
        $valid = true;
        if($this->FileEmpty($model,$media)){
            if(!$this->GetFileType($model,$media,'image')){
                $model->addError($media,"It seems you Didn't upload a proper image file ..");
                $valid=false;
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }
    public function validateVideo($model,$media,$platform)
    {

        $valid = true;
        if($this->FileEmpty($model,$media)){

            if(!$this->GetFileType($model,$media,'video','upload')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){
                    if($this->SizeVideo($model,$media,536870912,140,true)){
                        $valid = true;
                    }else{
                        $valid =false;
                    }
                }else{
                    $valid = true;
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function validateVideoUploader($model,$media,$platform,$name)
    {

        $valid = true;
        if($this->FileEmpty($model,$media)){

            if(!$this->GetFileType($model,$media,'video','upload')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){
                    if($this->SizeVideo($model,$media,536870912,140,true)){
                        $model->$media = $this->uploader($model,$media,$name,'video');
                        if($model->$media == false){
                            $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                            $valid=false;
                        }
                    }else{
                        $valid =false;
                    }
                }else{
                    $model->$media = $this->uploader($model,$media,$name,'video');
                    if($model->$media == false){
                        $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                        $valid=false;
                    }
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function validateVideoUpdate($model,$media,$platform,$name)
    {
        $valid = true;
        if($this->FileEmpty($model,$media)){
            if(!$this->GetFileType($model,$media,'video','upload')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){
                    if($this->SizeVideo($model,$media,536870912,140,true)){
                        $valid =true;
                    }else{
                        $valid =false;
                    }
                }else{
                    $valid =true;
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function validateDownloadVideoUploader($model,$media,$platform,$name,$type,$type_value)
    {
        $valid = true;

        if($this->FileIsExist($model->$media)){
            if(!$this->GetFileType($model,$model->$media,'video')){
                $model->addError($media,"It seems you Didn't upload a proper video file ..");
                $valid=false;
            }else{
                if($platform == 'Twitter'){

                    if($this->SizeVideo($model,$media,536870912,140)){
                        list($valid , $model->$media) = $this->uploaderFile($model->$media,$name);
                        if($model->$media == false){
                            $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                            $valid=false;
                        }
                    }else{
                        $model->$type = $type_value;
                        $model->addError($type_value,$model->getError($media));
                        /*                  $model->*/
                        $valid =false;
                    }
                }else{
                    list($valid , $model->$media) = $this->uploaderFile($model->$media,$name);
                    if($model->$media == false){
                        $model->addError($media,"<b>oops!!! :( </b><br>Something went wrong .<br>Please try again.");
                        $valid=false;
                    }
                }
            }
        }else{
            $model->addError($media,"Media cannot be blank. *required.");
            $valid=false;
        }
        return $valid;
    }

    public function getTweetLength($tweet, $gotImage ,$video) {
        $tcoLengthHttp = 23;
        $tcoLengthHttps = 23;
        $twitterPicLength = 24;
        $twitterVideoLength = 24;
        $urlData = Twitter_Extractor::create($tweet)->extract();
        $tweetLength = mb_strlen($tweet,'utf-8');
        foreach ($urlData['urls_with_indices'] as $url) {
            $tweetLength -= mb_strlen($url['url']);
            $tweetLength += mb_stristr($url['url'], 'https') === 0 ? $tcoLengthHttps : $tcoLengthHttp;
        }
        if ($gotImage) $tweetLength += $twitterPicLength;
        if ($video) $tweetLength += $twitterVideoLength;
        return $tweetLength;
    }

    public function shorten($input, $length, $ellipses = true, $strip_html = true) {
        if ($strip_html) {
            $input = strip_tags($input);
        }
        if (mb_strlen($input,'UTF-8') <= $length) {
            return $input;
        }
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = mb_substr($input, 0, $last_space);
        if ($ellipses) {
            $trimmed_text .= '...';
        }
        return $trimmed_text;
    }

    protected function get_news_details($url)
    {
        $html = new SimpleHTMLDOM();

        $html_data = $html->file_get_html($url);

        $data['url']= $url;

        $data['column']= null;

        $data['link_md5'] = md5($url);

        $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        $data['image']['type']='image';

        if(isset($html_data->find('h1[class=articletitle]', 0)->innertext))
            $data['title']= $this->clear_tags($html_data->find('h1[class=articletitle]', 0)->innertext);
        else {
            if(isset($html_data->find('meta[property=og:title]',0)->content))
                $data['title'] =  $this->clear_tags($html_data->find('meta[property=og:title]',0)->content);
            else
                $data['title'] = $this->news->title;
        }

        if(isset($html_data->find('time[class=date]', 0)->datetime)){
            $data['date'] = $this->get_date($this->clear_tags($html_data->find('time[class=date]', 0)->datetime));
        }else{
            $data['date'] = $this->news->publishing_date;
        }


        if(isset($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext))
            $data['author'] = $this->clear_author($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext);
        else{
            if(isset($html_data->find('meta[name=author]',0)->content))
                $data['author'] = $this->clear_author($html_data->find('meta[name=author]',0)->content);
            elseif(isset($html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content))
                $data['author'] = $html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content;
            else
                $data['author'] = $this->news->creator;
        }

        $data['number_star']='';
        if(isset($html_data->find('span[id=star-raters-number]', 0)->innertext))
            $data['number_star'] = $this->clear_tags($html_data->find('span[id=star-raters-number]', 0)->innertext);

        $data['description']='';

        if(isset($html_data->find('div[id=articleContent]', 0)->innertext))
            $data['description']= $this->clear_tags($html_data->find('div[id=articleContent]', 0)->innertext);
        elseif(isset($html_data->find('div[id=article-body]', 0)->innertext))
            $data['description']= $this->clear_tags($html_data->find('div[id=article-body]', 0)->innertext);
        else
            $data['description']= $this->news->description;


        $data['description'] = str_replace('لقراءة مقالات سابقة للكاتب يرجى النقر على اسمه .', '',  $data['description']);
        $data['description'] = str_replace('يمكن إرسال سكيك على خدمة «خبرونا» ضمن تطبيقات «الإمارات اليوم» على الهواتف الذكية.', '',  $data['description']);
        $data['description'] = str_replace('نعتذر عن عدم نشر ملاحظات القراء غير المرفق بها اسم ورقم هاتف المرسل.', '',  $data['description']);
        $data['description'] = str_replace('sekeek@emaratalyoum.com', '',  $data['description']);

        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'twitter:image:src') !== false) {
                $data['image']['src'] = $item->getAttribute('content');
                $data['image']['type']='image';
            }
        }

        if($data['image']['src'] == 'http://cache.emaratalyoum.com/res/img/logo-1024x576.png'){
            $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        }
        if(isset($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext))
            $data['column']= $this->clear_tags($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext);


        $gallary = $html_data->find('div[class=container_12] div[class=grid_8 pull_right content] article div[class=inlinegallery] div[class=articleinlinegallery] ul[class=slides] li img');
        $counter=0;
        foreach ($gallary as $item) {
            $data['gallary']['type']='gallery';
            $data['gallary']['src'][$counter] =  Yii::app()->params['feedUrl'].$item->src;
            $counter++;
        }

        $data['description'] = trim($data['description']);

        return $data;
    }

    public function clear_tags($string){

        $string = strip_tags($string);
        $string = str_replace('&nbsp;','',$string);
        $string = str_replace('&raquo;','',$string);
        $string = str_replace('&laquo;','',$string);
        $string = str_replace('&quot;','',$string);
        $string = str_replace('nbsp;','',$string);
        $string = str_replace('&#x2018;','',$string);
        $string = str_replace('&#x2019;','',$string);
        $string = str_replace('&#x2013;','',$string);
        return $string;
    }

    public function get_date($date){

        return date('Y-m-d H:i:s',strtotime($date));

    }

    public function clear_author($string){

        $string = strip_tags($string);

        return $string;
    }


    public function get_image_details($url)
    {
        $html = new SimpleHTMLDOM();
        $html_data = $html->file_get_html($url);
        $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        $data['image']['type']='image';
        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'twitter:image:src') !== false) {
                $data['image']['src'] = $item->getAttribute('content');
                $data['image']['type']='image';
            }
        }
        if($data['image']['src'] == 'http://cache.emaratalyoum.com/res/img/logo-1024x576.png'){
            $data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
        }
        $gallary = $html_data->find('div[class=container_12] div[class=grid_8 pull_right content] article div[class=inlinegallery] div[class=articleinlinegallery] ul[class=slides] li img');
        $counter=0;
        foreach ($gallary as $item) {
            $data['gallary']['type']='gallery';
            $data['gallary']['src'][$counter] = Yii::app()->params['feedUrl'] . $item->src;
            $counter++;

        }
        return $data;
    }


    public function get_template($platform,$category){

        $temp  = PostTemplate::model()->findByAttributes(array(
            'platform_id'=>$platform->id,
            'catgory_id'=>$category,
        ));
        if(!empty($temp))
            return $temp;
        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition='( ( platform_id = '.$platform->id.' or platform_id is NUll ) and  catgory_id is NULL )';
        $temp  = PostTemplate::model()->find($cond);

        if(!empty($temp))
            return $temp;

        return Yii::app()->params['templates'];
    }

    public function shorten_point($input, $point = '.') {
        $input_ex = explode($point,$input);
        $data = array_chunk($input_ex,2);
        if(is_array($data)){

            return (implode('. '.PHP_EOL,$data[0])).'. ';
        }
        return $input;
    }


    public function generator(){


        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index=>$item) {
            $this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
            $this->hash_tag[$index]=' '.trim($item->title).' ';
        }

        $this->parent = null;



        if(isset($this->category->title))
            $this->category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_',$this->category->title));


        if(isset($this->sub_category->title))
            $this->sub_category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', isset($this->sub_category->title)));

        $this->source = Yii::app()->params['source'].' : ';

        if($this->category == '#'.'أعمدة')
            $this->source = Yii::app()->params['line'].' : ';


        $this->creator = false;

        if($this->news->creator == 'دبي - الإمارات اليوم')
            $this->creator =true;

        if($this->model->submit_to_all){
            foreach (Platform::model()->findAllByAttributes(array('deleted' => 0)) as $attribute) {
                $this->platform = $attribute;
                $this->post();
            }
        }else{

            foreach ($this->model->platforms as $item) {
                $this->platform = Platform::model()->findByPk($item);
                $this->post();
            }
        }

        $this->news->generated = 1;

        $this->news->save();
    }

    public function post(){

        $is_scheduled =1;

        $twitter_is_scheduled =false;

        $media= null;

        $temp = $this->get_template($this->platform,$this->news->category_id);

        $title = $this->news->title;

        $description = $this->shorten_point($this->news->description,'.');

        if($this->platform->title != 'Instagram'){
            $title = str_replace($this->hash_tag, $this->trim_hash, $this->news->title);
            $description = str_replace($this->hash_tag, $this->trim_hash, $this->shorten_point($this->news->description,'.'));
        }

        $title =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);
        $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

        $description =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);
        $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

        $shorten_url = $this->news->shorten_url;
        $full_creator = $this->source.$this->news->creator;
        if(!empty(strpos($this->news->creator,':')))
            $full_creator = $this->news->creator;

        $cre= $this->platform->title == 'Twitter'?($this->creator?$full_creator:null):$full_creator;


        $text = str_replace(array('[title]','[description]','[short_link]','[author]',),array($title,$description,$shorten_url,$cre),$temp['text']
        );

        if($this->platform->title == 'Facebook' or $this->platform->title == 'Twitter'){
            $text = str_replace('# ', '#', $text);
            $found = true;
            preg_match_all("/#([^\s]+)/", $text, $matches);
            if(isset($matches[0])){
                $matches[0] = array_reverse($matches[0]);
                $count = 0;
                foreach ($matches[0] as $hashtag){
                    if($count>=2)
                    {
                        $found = false;
                        $hashtag_without = str_replace('#','',$hashtag);
                        $hashtag_without = str_replace('_',' ',$hashtag_without);
                        $text = str_replace($hashtag,$hashtag_without,$text);

                    }
                    $count++;
                }

                if($count>=2) {
                    $found = false;
                }
            }

            if($found){
                $text = str_replace(array('[section]', '[sub_section]',), array($this->category, isset($this->sub_category->title)?$this->sub_category->title:null,), $text);
            }else{
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);
            }
        }elseif($this->platform->title=='Instagram'){
            $text = str_replace('# ', '#', $text);
            $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), $text);
        }


        if(!empty($this->model->image)){
            $newsMedia = MediaNews::model()->findByAttributes(array('media'=>$this->model->image));
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }
        }else{
            $newsMedia = MediaNews::model()->findAll('(type = "image" or type = "gallery") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $index_media= rand(0,count($newsMedia)-1);
                if(isset($newsMedia[$index_media])){
                    $media = $newsMedia[$index_media]->media;
                }
            }
        }
        if($this->platform->title == 'Twitter'){

            $newsMedia = MediaNews::model()->find('(type = "image") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }

            $text_twitter =$text;

            if($temp['type'] == 'Preview')
                if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                    $text =$text.PHP_EOL.$this->news->shorten_url;

            if($this->getTweetLength($text_twitter,$temp['type'] == 'Image'?true:false,$temp['type'] == 'Video'?true:false) > 141) {
                $is_scheduled = 0;
                $twitter_is_scheduled = true;
            }else{
                $twitter_is_scheduled =false;
                $text = $text_twitter.PHP_EOL;
                if($temp['type'] == 'Preview')
                    if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                        $text .=PHP_EOL.$this->news->shorten_url;
            }

        }else{
            if($twitter_is_scheduled){
                if(!$is_scheduled){
                    $is_scheduled = 1;
                }
            }
        }
        $text = str_replace('# #', '#', $text);
        $text = str_replace('##', '#', $text);
        $text = str_replace('&#8220;', '', $text);
        $text = str_replace('&#8221;', '', $text);
        $text = str_replace('&#8230;', '', $text);
        $text = str_replace('#8211;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&nbsp;', '', $text);
        $text = str_replace('&#160;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&ndash;', '', $text);
        if($this->platform->title != 'Instagram'){
            $text =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
            $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
        }

        $this->PostQueue = new PostQueue();
        $this->PostQueue->setIsNewRecord(true);
        $this->PostQueue->id= null;
        $this->PostQueue->type = $temp['type'];
        $this->PostQueue->post = $text;
        $this->PostQueue->schedule_date = $this->news->schedule_date;
        $this->PostQueue->catgory_id =  $this->news->category_id;
        $this->PostQueue->main_category_id =  $this->news->category_id;
        $this->PostQueue->link = $this->news->shorten_url;
        $this->PostQueue->is_posted = 0;
        $this->PostQueue->news_id = $this->news->id;
        $this->PostQueue->post_id =null;
        $this->PostQueue->media_url =$media;
        $this->PostQueue->settings ='general';
        $this->PostQueue->is_scheduled =$is_scheduled;
        $this->PostQueue->platform_id =$this->platform->id;
        $this->PostQueue->generated ='auto';
        $this->PostQueue->pinned =$this->model->pinned;
        $this->PostQueue->created_at =date('Y-m-d H:i:s');

        if($this->parent == null){
            $this->PostQueue->parent_id =null;
            if($this->PostQueue->save())
                $this->parent = $this->PostQueue->id;

        }else{
            $this->PostQueue->parent_id =$this->parent;
            $this->PostQueue->save();
        }


    }

    public function AddNews($data){

        $news = new News();
        $news->id = null;
        $news->title = $data['title'];
        $news->link = $data['link'];
        $news->description = $data['description'];
        $news->category_id = $data['category_id'];
        $news->sub_category_id = $data['sub_category_id'];
        $news->column = $data['column'];
        $news->creator = $data['creator'];
        $news->link_md5 = $data['link_md5'];
        $news->shorten_url = $data['shorten_url'];
        $news->publishing_date = empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->schedule_date =  empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->generated = 0;
        $news->setIsNewRecord(true);
        $news->created_at = date('Y-m-d H:i:s');

        if($news->save(false)){
            $media = new MediaNews();
            $media->id = null;
            $media->media = isset($data['image']['src'])?$data['image']['src']:null;
            $media->news_id = $news->id ;
            $media->setIsNewRecord(true);
            $media->save(false);

        }

        return array($news->id,isset($data['image']['src'])?$data['image']['src']:null);

    }
    public function mediaNews($id, $data)
    {

        $media = new MediaNews();
        if($data['type'] == 'image'){

            $media->command=false;
            $media->id=null;
            $media->news_id = $id;
            $media->media = $data['src'];
            $media->type=$data['type'];
            $media->setIsNewRecord(true);
            if(!$media->save()){

            }


        }elseif($data['type'] == 'gallery'){
            foreach ($data['src'] as $item) {
                $empty_media = MediaNews::model()->findByAttributes(array('news_id'=> $id,'media'=>$item));
                if(empty($empty_media)){
                    $media->id=null;
                    $media->news_id = $id;
                    $media->media = $item;
                    $media->type=$data['type'];
                    $media->setIsNewRecord(true);
                    if(!$media->save()){

                    }
                }

            }
        }

    }

    public function check_emarate($link){

        $emarate_link   = 'http://www.emaratalyoum.com/';
        $pos = strpos(' '.$link, $emarate_link );
        if(!$pos) {
            return false;
        }
        return true;
    }

}